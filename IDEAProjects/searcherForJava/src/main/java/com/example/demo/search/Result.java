package com.example.demo.search;

/**
 * Created with IntelliJ IDEA.
 * Description: 存放搜索结果
 * User: Administrator
 * Date: 2023-01-15
 * Time: 22:28
 */
public class Result {
    private String title;
    private String url;
    private String desc;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return "search.Result{" +
                "title='" + title + '\'' +
                ", url='" + url + '\'' +
                ", desc='" + desc + '\'' +
                '}';
    }
}
