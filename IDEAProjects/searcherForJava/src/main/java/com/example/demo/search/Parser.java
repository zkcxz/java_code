package com.example.demo.search;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created with IntelliJ IDEA.
 * Description: 解析api文档的内容，完成索引的制作
 * User: Administrator
 * Date: 2023-01-11
 * Time: 11:56
 */
public class Parser {
    //指定加载文档的路径
    private static final String INPUT_PATH = "D:/java_code/java-api/java8/docs/api/";

    private Index index = new Index();

    public void run() throws SQLException {
        //根据指定的路径，枚举出该路径下的所有html文件（包括子目录中的所有文件）
        List<File> fileList = new ArrayList<>();
        enumFile(INPUT_PATH, fileList);
        //打开对应的文件，读取文件内容,进行解析并构建索引
        long begin = System.currentTimeMillis();
        for(File file : fileList){
            //解析html文件
            System.out.println("开始解析:" + file.getAbsolutePath());
            parseHTML(file);
        }
        long end = System.currentTimeMillis();
        System.out.println("遍历文件加入到索引中消耗的时间:" + (end - begin) + "ms");
        //内存中构造的索引数据结构保存到指定文件中
        index.save();
    }

    //实现多线程制作索引
    public void runByThread() throws InterruptedException, SQLException {
        //根据指定的路径，枚举出该路径下的所有html文件（包括子目录中的所有文件）
        List<File> fileList = new ArrayList<>();
        enumFile(INPUT_PATH, fileList);
        //打开对应的文件，读取文件内容，进行解析并构建索引（引入线程池）
        CountDownLatch countDownLatch = new CountDownLatch(fileList.size());
        ExecutorService executorService = Executors.newFixedThreadPool(4);
        long begin = System.currentTimeMillis();
        for(File file : fileList){
            executorService.submit(new Runnable() {
                @Override
                public void run() {
                    //解析html文件
                    System.out.println("开始解析:" + file.getAbsolutePath());
                    try {
                        parseHTML(file);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    countDownLatch.countDown();
                }
            });
        }
        countDownLatch.await();
        long end = System.currentTimeMillis();
        System.out.println("遍历文件加入到索引中消耗的时间:" + (end - begin) + "ms");
        executorService.shutdown();  //手动销毁线程池中的线程
        //内存中构造的索引数据结构保存到指定文件中
        index.save();
    }

    private void parseHTML(File file) throws SQLException {
        //解析标题、正文、url
        String title = parseTitle(file);
        //String content = parseContent(file);
        String content = parseContentByRegex(file);
        String url = parseUrl(file);
        //将解析出的信息加入到索引中
        index.addDoc(title, url, content);
    }

    //解析url
    private String parseUrl(File file) {
        String part1 = "https://docs.oracle.com/javase/8/docs/api/";
        String part2 = file.getAbsolutePath().substring(INPUT_PATH.length());
        return part1 + part2;
    }

    //解析标题
    private String parseTitle(File file) {
        String name = file.getName();
        return name.substring(0, name.length() - ".html".length());
    }

    //解析正文
    private String parseContent(File file) {
        try {
            //手动将缓冲区设置成1M大小
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file), 1024 * 1024);
            boolean flag = true;
            StringBuilder content = new StringBuilder();
            while(true){
                int t = bufferedReader.read();
                if(t == -1){
                    break;
                }
                char ch = (char)t;
                if(flag){
                    if(ch == '<'){
                        flag = false;
                        continue;
                    }
                    if(ch == '\n' || ch == '\r'){
                        ch = ' ';
                    }
                    content.append(ch);
                }else{
                    if(ch == '>'){
                        flag = true;
                    }
                }
            }
            bufferedReader.close();  //记得文件的关闭
            return content.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    //通过正则解析正文
    private String parseContentByRegex(File file) {
        String content = readFile(file);
        content = content.replaceAll("<script.*?>(.*?)</script>", " ");
        content = content.replaceAll("<.*?>", " ");
        content = content.replaceAll("\\s+", " ");
        return content;
    }

    private String readFile(File file) {
        try {
            //手动将缓冲区设置成1M大小
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file), 1024 * 1024);
            StringBuilder content = new StringBuilder();
            while(true){
                int t = bufferedReader.read();
                if(t == -1){
                    break;
                }
                char ch = (char)t;
                if(ch == '\n' || ch == '\r'){
                    ch = ' ';
                }
                content.append(ch);
            }
            bufferedReader.close();  //记得文件的关闭
            return content.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    private void enumFile(String inputPath, List<File> fileList) {
        File rootPath = new File(inputPath);
        File[] files = rootPath.listFiles();
        for(File file : files){
            if(file.isDirectory()){
                enumFile(file.getAbsolutePath(), fileList);
            }else{
                if(file.getAbsolutePath().endsWith(".html")) {
                    fileList.add(file);
                }
            }
        }
    }

    public static void main(String[] args) throws InterruptedException, SQLException {
        Parser parser = new Parser();
        parser.runByThread();
    }
}
