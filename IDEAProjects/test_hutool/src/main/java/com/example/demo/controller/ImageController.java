package com.example.demo.controller;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;
import cn.hutool.captcha.generator.RandomGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-03-31
 * Time: 11:50
 */
@Controller
public class ImageController {

    private LineCaptcha lineCaptcha;

    private Logger logger = LoggerFactory.getLogger(ImageController.class);

    /**
     * 登录逻辑实现
     * @param request
     * @return
     */
    @RequestMapping("/login")
    public String index(HttpServletRequest request) {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String imgText = request.getParameter("imgText");
        logger.info("username:{}", username);
        logger.info("password:{}", password);
        logger.info("登录验证码:{}", lineCaptcha.getCode());
        if (("user").equals(username) && ("123").equals(password) && imgText.equals(lineCaptcha.getCode())) {
            return "redirect:hello.html";
        } else {
            return "redirect:login.html";
        }
    }

    /**
     * 生成验证码
     * @param response
     */
    @RequestMapping("/getCode")
    public void getCode(HttpServletResponse response) {
        // 随机生成 4 位验证码
        RandomGenerator randomGenerator = new RandomGenerator("0123456789", 4);
        // 定义图片的显示大小
        lineCaptcha = CaptchaUtil.createLineCaptcha(100, 30);
        response.setContentType("image/jpeg");
        response.setHeader("Pragma", "No-cache");
        try {
            // 调用父类的 setGenerator() 方法，设置验证码的类型
            lineCaptcha.setGenerator(randomGenerator);
            // 输出到页面
            lineCaptcha.write(response.getOutputStream());
            // 打印日志
            logger.info("生成的验证码:{}", lineCaptcha.getCode());
            // 关闭流
            response.getOutputStream().close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
