package com.example.demo.controller;

import cn.hutool.core.io.FileUtil;
import cn.hutool.extra.qrcode.QrCodeUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-03-31
 * Time: 16:38
 */
@RestController
public class QRCodeController {
    @RequestMapping("/getQRCode")
    public void getQRCode(String url, int width, int height) {
        QrCodeUtil.generate(url, width, height, FileUtil.file("d:/qrcode.jpg"));
        String decode = QrCodeUtil.decode(FileUtil.file("d:/qrcode.jpg"));
        System.out.println(decode);
    }
}
