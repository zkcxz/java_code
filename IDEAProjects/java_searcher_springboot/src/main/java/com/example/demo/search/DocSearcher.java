package com.example.demo.search;

import org.ansj.domain.Term;
import org.ansj.splitWord.analysis.ToAnalysis;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description: 完成整个搜索的过程
 * User: Administrator
 * Date: 2023-01-15
 * Time: 22:26
 */
public class DocSearcher {
    private static String STOP_WORD_PATH = "D:/java_code/java-api/java8/stop_word.txt";

    static {
        if(Config.isOnline){
            STOP_WORD_PATH = "/root/development_project/doc_searcher_index/stop_word.txt";
        }else{
            STOP_WORD_PATH = "D:/java_code/java-api/java8/stop_word.txt";
        }
    }

    private Set<String> stopWords = new HashSet<>();

    private Index index = new Index();

    public DocSearcher(){
        index.load();  //启动时完成索引的加载
        loadStopWords();  //加载停用词表
    }

    public List<Result> search(String query){
        //分词
        List<Term> oldTerms = ToAnalysis.parse(query).getTerms();
        List<Term> terms = new ArrayList<>();
        //过滤
        for(Term term : oldTerms){
            if(stopWords.contains(term.getName())){
                continue;
            }
            terms.add(term);
        }
        //触发
        List<List<Weight>> termResult = new ArrayList<>();
        for(Term term : terms){
            String word = term.getName();
            List<Weight> invertedList = index.getInverted(word);
            if(invertedList == null){
                continue;
            }
            termResult.add(invertedList);
        }
        //合并
        List<Weight> allTermResult = mergeResult(termResult);
        //排序（降序）
        allTermResult.sort((o1, o2) -> {
            return o2.getWeight() - o1.getWeight();
        });
        //包装
        List<Result> results = new ArrayList<>();
        for(Weight weight : allTermResult){
            DocInfo docInfo = index.getDocInfo(weight.getDocId());
            Result result = new Result();
            result.setTitle(docInfo.getTitle());
            result.setUrl(docInfo.getUrl());
            result.setDesc(GenDesc(docInfo.getContent(), terms));
            results.add(result);
        }
        return results;
    }

    static class Pos{
        public int row;
        public int col;
        public Pos(int row, int col){
            this.row = row;
            this.col = col;
        }
    }

    private List<Weight> mergeResult(List<List<Weight>> source) {
        //针对每一行进行排序（按照id升序排序）
        for(List<Weight> curRow : source){
            curRow.sort(new Comparator<Weight>(){
                @Override
                public int compare(Weight o1, Weight o2) {
                    return o1.getDocId() - o2.getDocId();
                }
            });
        }
        //使用优先级队列对每一行进行合并
        List<Weight> target = new ArrayList<>();
        PriorityQueue<Pos> queue = new PriorityQueue<>(new Comparator<Pos>() {
            @Override
            public int compare(Pos o1, Pos o2) {
                return source.get(o1.row).get(o1.col).getDocId() - source.get(o2.row).get(o2.col).getDocId();
            }
        });
        for(int row = 0; row < source.size(); row++){
            queue.offer(new Pos(row, 0));
        }
        while(!queue.isEmpty()){
            Pos minPos = queue.poll();
            Weight curWeight = source.get(minPos.row).get(minPos.col);
            if(target.size() > 0){
                Weight lastWeight = target.get(target.size() - 1);
                if(lastWeight.getDocId() == curWeight.getDocId()){
                    lastWeight.setWeight(lastWeight.getWeight() + curWeight.getWeight());
                }else{
                    target.add(curWeight);
                }
            }else{
                target.add(curWeight);
            }
            Pos newPos = new Pos(minPos.row, minPos.col + 1);
            if(newPos.col >= source.get(newPos.row).size()){
                continue;
            }
            queue.offer(newPos);
        }
        return target;
    }

    private String GenDesc(String content, List<Term> terms) {
        int firstPos = -1;
        for(Term term : terms){
            String word = term.getName();
            content = content.toLowerCase().replaceAll("\\b" + word + "\\b", " " + word + " ");
            firstPos = content.indexOf(" " + word + " ");
            if(firstPos >= 0){
                break;
            }
        }
        if(firstPos == -1){
            return content.substring(0, Math.min(60, content.length())) + "...";
        }
        String desc = "";
        int descBeg = firstPos < 30 ? 0 : firstPos - 30;
        desc = content.substring(descBeg, Math.min(descBeg + 100, content.length())) + "...";
        for(Term term : terms){
            String word = term.getName();
            desc = desc.replaceAll("(?i) " + word + " ", "<i>" + " " + word + " " + "</i>");
        }
        return desc;
    }

    public void loadStopWords(){
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(STOP_WORD_PATH));
            while(true){
                String line = bufferedReader.readLine();
                if(line == null){
                    break;
                }
                stopWords.add(line);
            }
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        DocSearcher docSearcher = new DocSearcher();
        Scanner scanner = new Scanner(System.in);
        while(true){
            System.out.print("->");
            String query = scanner.next();
            List<Result> results = docSearcher.search(query);
            for(Result result : results){
                System.out.println("==============================");
                System.out.println(result);
            }
        }
    }
}
