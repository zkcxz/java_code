package search;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.ansj.domain.Term;
import org.ansj.splitWord.analysis.ToAnalysis;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description: 在内存中构建索引结构
 * User: Administrator
 * Date: 2023-01-11
 * Time: 15:20
 */
public class Index {
    private static final String INDEX_PATH = "D:/java_code/java-api/java8/";
    private ObjectMapper objectMapper = new ObjectMapper();

    private List<DocInfo> forwardIndex = new ArrayList<>();  //正排索引
    private Map<String, List<Weight>> invertedIndex = new HashMap<>();  //倒排索引

    private Object locker1 = new Object();
    private Object locker2 = new Object();
    private Object locker3 = new Object();
    private Object locker4 = new Object();

    //给定一个docId，通过正排索引查询到文档的详细信息
    public DocInfo getDocInfo(int docId){
        return forwardIndex.get(docId);
    }

    //给定一个词，通过倒排索引查询哪些文档与这个词相关联
    public List<Weight> getInverted(String term){
        return invertedIndex.get(term);
    }

    //往索引中新增一个文档
    public void addDoc(String title, String url, String content){
        DocInfo docInfo = buildForward(title, url, content);
        buildInverted(docInfo);
    }

    private void buildInverted(DocInfo docInfo) {
        //针对标题和正文进行分词（需要分开计算），并统计每个词出现的次数
        Map<String, WordCnt> wordCntMap = new HashMap<>();
        List<Term> terms = ToAnalysis.parse(docInfo.getTitle()).getTerms();
        for(Term term : terms){
            synchronized (locker1){
                String word = term.getName();
                if(!wordCntMap.containsKey(word)){
                    WordCnt wordCnt = new WordCnt();
                    wordCnt.setTitleCount(1);
                    wordCnt.setContentCount(0);
                    wordCntMap.put(word, wordCnt);
                }else{
                    WordCnt wordCnt = wordCntMap.get(word);
                    wordCnt.setTitleCount(wordCnt.getTitleCount() + 1);
                }
            }
        }
        terms = ToAnalysis.parse(docInfo.getContent()).getTerms();
        for(Term term : terms){
            synchronized (locker2){
                String word = term.getName();
                if(!wordCntMap.containsKey(word)){
                    WordCnt wordCnt = new WordCnt();
                    wordCnt.setTitleCount(0);
                    wordCnt.setContentCount(1);
                    wordCntMap.put(word, wordCnt);
                }else{
                    WordCnt wordCnt = wordCntMap.get(word);
                    wordCnt.setContentCount(wordCnt.getContentCount() + 1);
                }
            }
        }
        for(Map.Entry<String, WordCnt> entry : wordCntMap.entrySet()){
            synchronized (locker3){
                List<Weight> invertedList = invertedIndex.get(entry.getKey());
                if(invertedList == null){
                    List<Weight> list = new ArrayList<>();
                    Weight weight = new Weight();
                    weight.setDocId(docInfo.getDocId());
                    weight.setWeight(entry.getValue().getTitleCount() * 10 + entry.getValue().getContentCount());
                    list.add(weight);
                    invertedIndex.put(entry.getKey(), list);
                }else{
                    Weight weight = new Weight();
                    weight.setDocId(docInfo.getDocId());
                    weight.setWeight(entry.getValue().getTitleCount() * 10 + entry.getValue().getContentCount());
                    invertedList.add(weight);
                }
            }
        }
    }

    private DocInfo buildForward(String title, String url, String content) {
        DocInfo docInfo = new DocInfo();
        docInfo.setTitle(title);
        docInfo.setUrl(url);
        docInfo.setContent(content);
        synchronized (locker4){
            docInfo.setDocId(forwardIndex.size());
            forwardIndex.add(docInfo);
        }
        return docInfo;
    }

    //把内存中的索引结构保存到磁盘中
    public void save(){
        //使用两个文件分别表示正排和倒排
        //判断索引对应的文件是否存在，不存在就创建
        long begin = System.currentTimeMillis();
        File indexPathFile = new File(INDEX_PATH);
        if(!indexPathFile.exists()){
            indexPathFile.mkdirs();
        }
        File forwardIndexFile = new File(INDEX_PATH + "forward.txt");
        File invertedIndexFile = new File(INDEX_PATH + "inverted.txt");
        try {
            objectMapper.writeValue(forwardIndexFile, forwardIndex);
            objectMapper.writeValue(invertedIndexFile, invertedIndex);
        } catch (IOException e) {
            e.printStackTrace();
        }
        long end = System.currentTimeMillis();
        System.out.println("保存索引成功. 消耗时间:" + (end - begin) + "ms");
    }

    //把磁盘中的索引数据加载到内存中
    public void load(){
        long begin = System.currentTimeMillis();
        File forwardIndexFile = new File(INDEX_PATH + "forward.txt");
        File invertedIndexFile = new File(INDEX_PATH + "inverted.txt");
        try {
            forwardIndex = objectMapper.readValue(forwardIndexFile, new TypeReference<List<DocInfo>>() {});
            invertedIndex = objectMapper.readValue(invertedIndexFile, new TypeReference<Map<String, List<Weight>>>() {});
        } catch (IOException e) {
            e.printStackTrace();
        }
        long end = System.currentTimeMillis();
        System.out.println("数据加载成功. 消耗时间:" + (end - begin) + "ms");
    }

    public static void main(String[] args) {
        Index index = new Index();
        index.load();
        System.out.println("索引加载完成.");
    }
}
