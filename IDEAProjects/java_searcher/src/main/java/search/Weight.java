package search;

/**
 * Created with IntelliJ IDEA.
 * Description: 将文档id和文档与词的相关性的权重进行整合
 * User: Administrator
 * Date: 2023-01-11
 * Time: 15:31
 */
public class Weight {
    private int docId;
    private int weight;

    public int getDocId() {
        return docId;
    }

    public void setDocId(int docId) {
        this.docId = docId;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
