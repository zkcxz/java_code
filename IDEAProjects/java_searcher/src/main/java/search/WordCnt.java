package search;

/**
 * Created with IntelliJ IDEA.
 * Description: 统计词频
 * User: Administrator
 * Date: 2023-01-11
 * Time: 16:14
 */
public class WordCnt {
    private int titleCount;
    private int contentCount;

    public int getTitleCount() {
        return titleCount;
    }

    public void setTitleCount(int titleCount) {
        this.titleCount = titleCount;
    }

    public int getContentCount() {
        return contentCount;
    }

    public void setContentCount(int contentCount) {
        this.contentCount = contentCount;
    }
}
