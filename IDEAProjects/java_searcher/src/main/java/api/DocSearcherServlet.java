package api;

import com.fasterxml.jackson.databind.ObjectMapper;
import search.DocSearcher;
import search.Result;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-01-16
 * Time: 15:43
 */
@WebServlet("/searcher")
public class DocSearcherServlet extends HttpServlet {
    private static DocSearcher docSearcher = new DocSearcher();
    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String query = req.getParameter("query");
        if(query == null || query.equals("")){
            resp.sendError(404);
            return;
        }
        System.out.println("query=" + query);
        List<Result> results = docSearcher.search(query);
        resp.setContentType("application/json;charset=utf-8");
        objectMapper.writeValue(resp.getWriter(), results);
    }
}
