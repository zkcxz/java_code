import org.ansj.domain.Term;
import org.ansj.splitWord.analysis.ToAnalysis;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-01-10
 * Time: 23:51
 */
public class TestAnsj {
    public static void main(String[] args) {
        String str = "由于这是个人使用, 所以有一些题目导入之后的描述可能会出现不清楚或者歧义的情况~";
        List<Term> terms = ToAnalysis.parse(str).getTerms();  //Term表示一个分词结果
        for(Term term : terms){
            System.out.println(term.getName());
        }
    }
}
