package com.example.onlinemusicserver.model;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-03-07
 * Time: 12:07
 */
@Data
public class Music {
    private int id;
    private String title;
    private String singer;
    private String time;
    private String url;
    private int uid;
}
