package com.example.onlinemusicserver.mapper;

import com.example.onlinemusicserver.model.Music;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-03-10
 * Time: 20:56
 */
@Mapper
public interface LoveMusicMapper {
    public Music findLoveMusic(int uid, int mid);

    public boolean insertLoveMusic(int uid, int mid);

    public List<Music> findLoveMusicByUserId(int uid);

    public List<Music> findLoveMusicByKeyAndUserId(String name, int uid);

    public int deleteLoveMusic(int uid, int mid);

    public int deleteLoveMusicByMId(int mid);
}
