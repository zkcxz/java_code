package com.example.onlinemusicserver.mapper;

import com.example.onlinemusicserver.model.Music;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-03-07
 * Time: 13:26
 */
@Mapper
public interface MusicMapper {
    public int insert(String title, String singer, String url, int userid);

    public Integer check(String title, String singer);

    public Music findMusicById(int id);

    public int deleteMusicById(int id);

    List<Music> findMusic();

    List<Music> findMusicByName(String name);
}
