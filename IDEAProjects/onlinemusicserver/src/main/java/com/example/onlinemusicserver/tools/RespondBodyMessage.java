package com.example.onlinemusicserver.tools;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-03-05
 * Time: 21:29
 */
@Data
public class RespondBodyMessage <T>{
    private int status;  //状态码
    private String message;  //返回信息
    private T data;  //返回数据

    public RespondBodyMessage(int status, String message, T data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }
}
