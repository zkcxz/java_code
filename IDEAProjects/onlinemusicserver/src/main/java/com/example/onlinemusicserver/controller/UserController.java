package com.example.onlinemusicserver.controller;

import com.example.onlinemusicserver.mapper.UserMapper;
import com.example.onlinemusicserver.model.User;
import com.example.onlinemusicserver.tools.Constant;
import com.example.onlinemusicserver.tools.RespondBodyMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-03-05
 * Time: 21:08
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @RequestMapping("/reg")
    public RespondBodyMessage<Boolean> reg(@RequestParam String username, @RequestParam String password){
        //非空效验
        if(!StringUtils.hasLength(username) || !StringUtils.hasLength(password)){
            return new RespondBodyMessage<>(-1, "非法的参数请求", false);
        }
        User user = userMapper.findByName(username);
        if(user != null){
            return new RespondBodyMessage<>(-1, "该用户名已被注册！", false);
        }else{
            String newPassword = bCryptPasswordEncoder.encode(password);
            int res = userMapper.add(username, newPassword);
            if(res == 1){
                return new RespondBodyMessage<>(200, "添加用户成功！", true);
            }else{
                return new RespondBodyMessage<>(-1, "数据库添加失败！", false);
            }
        }
    }

    @RequestMapping("/login")
    public RespondBodyMessage<User> login(HttpServletRequest request, @RequestParam String username, @RequestParam String password){
        //非空效验
        if(!StringUtils.hasLength(username) || !StringUtils.hasLength(password)){
            return new RespondBodyMessage<>(-1, "非法的参数请求", null);
        }
        User user = userMapper.findByName(username);
        if(user == null){
            System.out.println("登录失败");
            return new RespondBodyMessage<>(-1, "登录失败！", null);
        }else{
            boolean flag = bCryptPasswordEncoder.matches(password, user.getPassword());
            if(flag == false){
                return new RespondBodyMessage<>(-1, "用户名或密码错误！", null);
            }
            System.out.println("登录成功");
            request.getSession().setAttribute(Constant.USERINFO_SESSION_KEY, user);
            return new RespondBodyMessage<>(200, "登录成功！", user);
        }
    }
}
