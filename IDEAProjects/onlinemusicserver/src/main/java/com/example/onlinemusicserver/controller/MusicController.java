package com.example.onlinemusicserver.controller;

import com.example.onlinemusicserver.mapper.LoveMusicMapper;
import com.example.onlinemusicserver.mapper.MusicMapper;
import com.example.onlinemusicserver.model.Music;
import com.example.onlinemusicserver.model.User;
import com.example.onlinemusicserver.tools.Constant;
import com.example.onlinemusicserver.tools.RespondBodyMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-03-07
 * Time: 12:11
 */
@RestController
@RequestMapping("/music")
public class MusicController {
    @Value("${music.local.path}")
    private String SAVE_PATH;

    @Autowired
    private MusicMapper musicMapper;

    @Autowired
    private LoveMusicMapper loveMusicMapper;

    @RequestMapping("/upload")
    public RespondBodyMessage<Boolean> insertMusic(HttpServletRequest request,
                                                   HttpServletResponse resp,
                                                   @RequestParam String singer,
                                                   @RequestParam("filename") MultipartFile file){
        HttpSession session = request.getSession(false);
        if(session != null && session.getAttribute(Constant.USERINFO_SESSION_KEY) != null){
            String fileName = file.getOriginalFilename();
            int index = fileName.lastIndexOf(".");
            String title = fileName.substring(0, index);
            Integer cnt = musicMapper.check(title, singer);
            if(cnt != null){
                return new RespondBodyMessage<>(-1, "已存在该歌曲！", false);
            }
            String path = SAVE_PATH + fileName;
            File dest = new File(path);
            if(!dest.exists()){
                dest.mkdirs();
            }
            try {
                //将文件传输到服务器
                file.transferTo(dest);
                //将文件信息存放到数据库
                User user = (User) session.getAttribute(Constant.USERINFO_SESSION_KEY);
                int uid = user.getId();
                String url = "/music/get?path=" + title;
                int res = musicMapper.insert(title, singer, url, uid);
                if(res == 1){
                    resp.sendRedirect("/list.html");
                }else{
                    return new RespondBodyMessage<>(-1, "数据库上传失败！", false);
                }
                return new RespondBodyMessage<>(200, "上传成功！", true);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return new RespondBodyMessage<>(-1, "上传失败！", false);
        }
        return new RespondBodyMessage<>(-1, "请先登录！", false);
    }

    @RequestMapping("/get")
    public ResponseEntity<byte[]> get(String path){
        File file = new File(SAVE_PATH + path);
        byte[] a = null;
        try {
            a = Files.readAllBytes(file.toPath());
            if(a == null){
                return ResponseEntity.badRequest().build();
            }
            return ResponseEntity.ok(a);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResponseEntity.badRequest().build();
    }

    @RequestMapping("/delete")
    public RespondBodyMessage<Boolean> deleteMusicById(@RequestParam int id){
        Music music = musicMapper.findMusicById(id);
        if(music == null){
            return new RespondBodyMessage<>(-1, "没有你要删除的音乐！", false);
        }else{
            //删除数据库
            int res = musicMapper.deleteMusicById(id);
            if(res == 1){
                //删除服务器
                String title = music.getTitle();
                File file = new File(SAVE_PATH + title + ".mp3");
                if(file.delete()){
                    //同步删除lovemusic表中的音乐
                    loveMusicMapper.deleteLoveMusicByMId(id);
                    return new RespondBodyMessage<>(200, "删除音乐成功！", true);
                }else{
                    return new RespondBodyMessage<>(-1, "删除音乐失败！", false);
                }
            }else{
                return new RespondBodyMessage<>(-1, "数据库删除失败！", false);
            }
        }
    }

    @RequestMapping("/deleteSel")
    public RespondBodyMessage<Boolean> deleteSelMusic(@RequestParam("id[]") List<Integer> id){
        int sum = 0;
        for(int x : id){
            Music music = musicMapper.findMusicById(x);
            if(music == null){
                return new RespondBodyMessage<>(-1, "没有你要删除的音乐！", false);
            }
            //删除数据库
            int res = musicMapper.deleteMusicById(x);
            if(res == 1) {
                //删除服务器
                String title = music.getTitle();
                File file = new File(SAVE_PATH + title + ".mp3");
                if (file.delete()) {
                    //同步删除lovemusic表中的音乐
                    loveMusicMapper.deleteLoveMusicByMId(x);
                    sum += res;
                } else {
                    return new RespondBodyMessage<>(-1, "服务器删除音乐失败！", false);
                }
            }else{
                return new RespondBodyMessage<>(-1, "数据库删除音乐失败！", false);
            }
        }
        if(sum == id.size()){
            return new RespondBodyMessage<>(200, "删除音乐成功！", true);
        }else{
            return new RespondBodyMessage<>(-1, "删除音乐失败！", false);
        }
    }

    @RequestMapping("/findMusic")
    public RespondBodyMessage<List<Music>> findMusic(@RequestParam(required = false) String name){
        List<Music> list = null;
        if(name != null){
            list = musicMapper.findMusicByName(name);
        }else{
            list = musicMapper.findMusic();
        }
        return new RespondBodyMessage<>(200, "查询完毕！", list);
    }
}
