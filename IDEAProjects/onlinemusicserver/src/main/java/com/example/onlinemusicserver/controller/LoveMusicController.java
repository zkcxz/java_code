package com.example.onlinemusicserver.controller;

import com.example.onlinemusicserver.mapper.LoveMusicMapper;
import com.example.onlinemusicserver.model.Music;
import com.example.onlinemusicserver.model.User;
import com.example.onlinemusicserver.tools.Constant;
import com.example.onlinemusicserver.tools.RespondBodyMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-03-10
 * Time: 21:29
 */
@RestController
@RequestMapping("/lovemusic")
public class LoveMusicController {
    @Autowired
    private LoveMusicMapper loveMusicMapper;

    @RequestMapping("/likeMusic")
    public RespondBodyMessage<Boolean> likeMusic(@RequestParam int id, HttpServletRequest request){
        HttpSession session = request.getSession(false);
        if(session != null && session.getAttribute(Constant.USERINFO_SESSION_KEY) == null){
            return new RespondBodyMessage<>(-1, "请先登录！", false);
        }
        User user = (User) session.getAttribute(Constant.USERINFO_SESSION_KEY);
        int uid = user.getId();
        int mid = id;
        Music music = loveMusicMapper.findLoveMusic(uid, mid);
        if(music != null){
            return new RespondBodyMessage<>(-1, "收藏过该音乐！", false);
        }
        boolean effect = loveMusicMapper.insertLoveMusic(uid, mid);
        if(effect){
            return new RespondBodyMessage<>(200, "收藏成功！",true);
        }else{
            return new RespondBodyMessage<>(-1, "收藏失败！", false);
        }
    }

    @RequestMapping("/findlovemusic")
    public RespondBodyMessage<List<Music>> findLoveMusic(@RequestParam(required = false) String name, HttpServletRequest request){
        HttpSession session = request.getSession(false);
        if(session != null && session.getAttribute(Constant.USERINFO_SESSION_KEY) == null){
            return new RespondBodyMessage<>(-1, "请先登录！", null);
        }
        User user = (User) session.getAttribute(Constant.USERINFO_SESSION_KEY);
        int uid = user.getId();
        List<Music> list = null;
        if(name == null){
            list = loveMusicMapper.findLoveMusicByUserId(uid);
        }else{
            list = loveMusicMapper.findLoveMusicByKeyAndUserId(name, uid);
        }
        return new RespondBodyMessage<>(200, "查询到所有歌曲信息！", list);
    }

    @RequestMapping("/deletelovemusic")
    public RespondBodyMessage<Boolean> deleteLoveMusic(@RequestParam int mid, HttpServletRequest request){
        HttpSession session = request.getSession(false);
        if(session != null && session.getAttribute(Constant.USERINFO_SESSION_KEY) == null){
            return new RespondBodyMessage<>(-1, "请先登录！", null);
        }
        User user = (User) session.getAttribute(Constant.USERINFO_SESSION_KEY);
        int uid = user.getId();
        int res = loveMusicMapper.deleteLoveMusic(uid, mid);
        if(res == 1){
            return new RespondBodyMessage<>(200, "取消收藏成功！", true);
        }else{
            return new RespondBodyMessage<>(-1, "取消收藏失败！", false);
        }
    }
}
