package com.example.onlinemusicserver.model;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-03-05
 * Time: 14:18
 */
@Data
public class User {
    private int id;
    private String name;
    private String password;
}
