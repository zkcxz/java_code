package com.example.onlinemusicserver.mapper;

import com.example.onlinemusicserver.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-03-05
 * Time: 14:20
 */
@Mapper
public interface UserMapper {
    public User findByName(String username);

    public int add(String username, String password);
}
