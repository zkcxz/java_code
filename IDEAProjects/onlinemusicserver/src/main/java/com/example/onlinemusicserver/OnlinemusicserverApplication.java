package com.example.onlinemusicserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(exclude ={org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class})
public class OnlinemusicserverApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlinemusicserverApplication.class, args);
	}

}
