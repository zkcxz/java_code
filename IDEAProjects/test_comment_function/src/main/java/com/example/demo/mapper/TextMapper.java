package com.example.demo.mapper;

import com.example.demo.model.Message;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-03-30
 * Time: 16:40
 */
@Mapper
public interface TextMapper {
    public Integer insert(@Param("from") String from, @Param("to") String to, @Param("message") String message);

    public List<Message> load();
}
