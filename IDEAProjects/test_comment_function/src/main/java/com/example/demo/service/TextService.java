package com.example.demo.service;

import com.example.demo.mapper.TextMapper;
import com.example.demo.model.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-03-30
 * Time: 15:35
 */
@Service
public class TextService {
    @Autowired
    private TextMapper testMapper;

    public int insert(String from, String to, String message){
        return testMapper.insert(from, to, message);
    }

    public List<Message> load(){
        return testMapper.load();
    }
}
