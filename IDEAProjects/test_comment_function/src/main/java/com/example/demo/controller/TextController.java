package com.example.demo.controller;

import com.example.demo.model.Message;
import com.example.demo.service.TextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-03-30
 * Time: 15:07
 */
@RestController
public class TextController {
    @Autowired
    private TextService textService;

    @RequestMapping("/upload")
    public int upload(String from, String to, String message){
        if(textService.insert(from, to, message) != 0){
            return 200;
        }
        return 400;
    }

    @RequestMapping("/load")
    public List<Message> load(){
        return textService.load();
    }
}
