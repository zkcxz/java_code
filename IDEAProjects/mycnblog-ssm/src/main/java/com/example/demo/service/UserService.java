package com.example.demo.service;

import com.example.demo.mapper.UserMapper;
import com.example.demo.model.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * Description: 用户表的服务层
 * User: Administrator
 * Date: 2023-01-08
 * Time: 21:42
 */
@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;

    public Integer findName(String username){
        return userMapper.findName(username);
    }

    public int add(String username, String password){
        return userMapper.add(username, password);
    }

    public UserInfo login(String username, String password){
        return userMapper.login(username, password);
    }

    public UserInfo getUserByName(String username){
        return userMapper.getUserByName(username);
    }

    public UserInfo myInfoById(Integer uid){
        return userMapper.myInfoById(uid);
    }

    public int updatePhoto(String fileName, Integer id){
        return userMapper.updatePhoto(fileName, id);
    }
}
