package com.example.demo.controller;

import com.example.demo.common.AjaxResult;
import com.example.demo.common.Constant;
import com.example.demo.common.SecurityUtil;
import com.example.demo.model.UserInfo;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description: 用户控制器
 * User: Administrator
 * Date: 2023-01-08
 * Time: 21:39
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping("/reg")
    public Object reg(String username, String password){
        //非空效验
        if(!StringUtils.hasLength(username) || !StringUtils.hasLength(password)){
            return AjaxResult.fail(-1, "非法的参数请求");
        }
        //判断数据库中是否存在该用户
        Integer reg = userService.findName(username);
        if(reg == null) {
            //进行添加操作
            int res = userService.add(username, SecurityUtil.encrypt(password));
            if (res == 1) {
                return AjaxResult.success("添加成功！", 1);
            } else {
                return AjaxResult.fail(-1, "数据库添加出错");
            }
        }
        return AjaxResult.fail(-1, "该用户名已存在");
    }

    @RequestMapping("/login")
    public int login(HttpServletRequest request, String username, String password){
        //非空效验
        if(!StringUtils.hasLength(username) || !StringUtils.hasLength(password)){
            return 0;
        }
        //进行判断操作
        UserInfo userInfo = userService.getUserByName(username);
        if(userInfo == null || userInfo.getId() < 0){
            return 0;
        }else{
            if(SecurityUtil.decrypt(password, userInfo.getPassword())){
                //将userinfo保存到session中
                HttpSession session = request.getSession();
                session.setAttribute(Constant.SESSION_USERINFO_KEY, userInfo);
                return 1;
            }
        }
        return 0;
    }

    @RequestMapping("/myinfo")
    public UserInfo myInfo(HttpServletRequest request){
        HttpSession session = request.getSession(false);
        if(session != null && session.getAttribute(Constant.SESSION_USERINFO_KEY) != null){
            return (UserInfo) session.getAttribute(Constant.SESSION_USERINFO_KEY);
        }
        return null;
    }

    @RequestMapping("/authentication")
    public UserInfo authentication(HttpServletRequest request){
        HttpSession session = request.getSession(false);
        if(session != null && session.getAttribute(Constant.SESSION_USERINFO_KEY) != null){
            return (UserInfo) session.getAttribute(Constant.SESSION_USERINFO_KEY);
        }
        return null;
    }

    @RequestMapping("/myinfobyid")
    public Object myInfoById(HttpServletRequest request, Integer uid){
        HttpSession session = request.getSession(false);
        if(session != null && session.getAttribute(Constant.SESSION_USERINFO_KEY) != null){
            UserInfo userInfo = (UserInfo) session.getAttribute(Constant.SESSION_USERINFO_KEY);
            if(userInfo.getId() == uid){
                return AjaxResult.success("1", userInfo);
            }
        }
        UserInfo userInfo = userService.myInfoById(uid);
        return AjaxResult.success("0", userInfo);
    }

    @RequestMapping("/logout")
    public boolean logout(HttpServletRequest request){
        HttpSession session = request.getSession(false);
        if(session != null && session.getAttribute(Constant.SESSION_USERINFO_KEY) != null){
            session.removeAttribute(Constant.SESSION_USERINFO_KEY);
        }
        return true;
    }

    @RequestMapping("/upload")
    public Object upload(HttpServletRequest request, MultipartFile file){
        HttpSession session = request.getSession( false);
        UserInfo userInfo = null;
        if(session != null && session.getAttribute(Constant.SESSION_USERINFO_KEY) != null) {
            userInfo = (UserInfo) session.getAttribute(Constant.SESSION_USERINFO_KEY);
        }
        if(file.isEmpty()){
            return AjaxResult.fail(-1, "请选择图片");
        }
        String originalFileName = file.getOriginalFilename();  //原来图片名
        String[] tmp = originalFileName.split("\\.");
//        if(tmp.length == 1 || !tmp[tmp.length - 1].equals("jpg") || !tmp[tmp.length - 1].equals("png")){
//            return AjaxResult.fail(-1, "图片格式错误");
//        }
        String ext = "." + tmp[tmp.length - 1];  //取到后缀
        String fileName = userInfo.getUsername() + ext;
        //上传图片
        String pre = "/www/wwwroot/47.115.218.229/blog_user_img/";
//        ApplicationHome applicationHome = new ApplicationHome(this.getClass());
//        String pre = applicationHome.getDir().getParentFile().getParentFile().getParentFile().getAbsolutePath() +
//                "/src/main/resources/static/img/";
        String path = pre + fileName;
        try {
            //存到外部文件夹中
            file.transferTo(new File(path));

            userService.updatePhoto(fileName, userInfo.getId());

            return AjaxResult.success("更换成功！", 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return AjaxResult.fail(-1, "更换失败");
    }
}
