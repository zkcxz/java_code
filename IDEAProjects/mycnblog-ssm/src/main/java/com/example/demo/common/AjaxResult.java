package com.example.demo.common;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * Description: 自定义统一返回对象
 * User: Administrator
 * Date: 2023-01-09
 * Time: 12:26
 */
public class AjaxResult {
    //业务执行成功时返回的方法
    public static HashMap<String, Object> success(Object data){
        HashMap<String, Object> res = new HashMap<>();
        res.put("code", 200);
        res.put("msg", "");
        res.put("data", data);
        return res;
    }

    public static HashMap<String, Object> success(String msg, Object data){
        HashMap<String, Object> res = new HashMap<>();
        res.put("code", 200);
        res.put("msg", msg);
        res.put("data", data);
        return res;
    }

    //业务执行失败时返回的方法
    public static HashMap<String, Object> fail(int code, String msg){
        HashMap<String, Object> res = new HashMap<>();
        res.put("code", code);
        res.put("msg", msg);
        res.put("data", "");
        return res;
    }

    public static HashMap<String, Object> fail(int code, String msg, Object data){
        HashMap<String, Object> res = new HashMap<>();
        res.put("code", code);
        res.put("msg", msg);
        res.put("data", data);
        return res;
    }
}
