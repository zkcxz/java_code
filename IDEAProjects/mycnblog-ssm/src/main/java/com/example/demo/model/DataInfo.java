package com.example.demo.model;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-12-25
 * Time: 12:22
 */
@Data
public class DataInfo {
    private int yy;
    private int cnt;
}
