package com.example.demo.mapper;

import com.example.demo.model.ArticleInfo;
import com.example.demo.model.DataInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description: 文章表 mapper
 * User: Administrator
 * Date: 2023-01-08
 * Time: 21:49
 */
@Mapper
public interface ArticleMapper {
    public List<ArticleInfo> getMyList(@Param("uid") Integer uid);

    public List<ArticleInfo> getList(@Param("psize") Integer psize, @Param("offset") Integer offset);

    public ArticleInfo getDetail(@Param("aid") Integer aid);

    public int deleteById(@Param("aid") Integer aid);

    public int update(@Param("aid") Integer aid, @Param("title") String title, @Param("content") String content);

    public int addArt(@Param("uid") Integer uid, @Param("title") String title, @Param("content") String content);

    public int getTotalCount();

    public int getCreateCount(@Param("uid") Integer uid);

    public int getReadCount(@Param("uid") Integer uid);

    public int getReadCountByAid(@Param("aid") Integer aid);

    public int addrcount(@Param("aid") Integer aid, @Param("newrcount") Integer newrcount);

    public List<ArticleInfo> getSearcherList(@Param("query") String query);

    public ArticleInfo gettopid(@Param("aid") Integer aid);

    public int top1byaid(@Param("aid") Integer aid);

    public int top0byaid(@Param("aid") Integer aid);

    public List<DataInfo> mydata(@Param("uid") int uid);

    public List<ArticleInfo> getSearcherListBytime(@Param("query") String query);

    public List<ArticleInfo> getSearcherListBynums(@Param("query") String query);
}
