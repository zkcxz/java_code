package com.example.demo.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description: 自定义拦截器
 * User: Administrator
 * Date: 2023-01-10
 * Time: 15:19
 */
@Configuration
public class AppConfig implements WebMvcConfigurer {
    //不拦截的url集合
    List<String> excludes = new ArrayList<String>(){{
        add("/**/*.html");
        add("/js/**");
        add("/css/**");
        add("/editor.md/**");
        add("/img/**");
        add("/user/login");
        add("/user/reg");
        add("/art/detail");
        add("/user/myinfobyid");
        add("/art/list");
        add("/art/totalpage");
        add("/user/authentication");
        add("/art/mycreate");
        add("/art/myrcount");
        add("/art/mycreatebyid");
        add("/art/myrcountbyid");
        add("/art/addrcount");
        add("/art/searcher");
        add("/art/gettopid");
        add("/art/mydata");
    }};

    @Autowired
    private LoginInterceptor loginInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginInterceptor)
                .addPathPatterns("/**")
                .excludePathPatterns(excludes);
    }
}
