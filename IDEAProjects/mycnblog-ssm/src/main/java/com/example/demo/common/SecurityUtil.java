package com.example.demo.common;

import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * Description: 加盐加密类
 * User: Administrator
 * Date: 2023-01-15
 * Time: 19:15
 */
public class SecurityUtil {
    //加密操作
    public static String encrypt(String password){
        //每次生成内容不同但长度固定为32位的盐值
        String salt = UUID.randomUUID().toString().replace("-", "");
        String finalPassword = DigestUtils.md5DigestAsHex((salt + password).getBytes());
        return salt + finalPassword;
    }

    //验证操作
    public static boolean decrypt(String password, String finalPassword){
        if(!StringUtils.hasLength(password) || !StringUtils.hasLength(finalPassword)){
            return false;
        }
        if(finalPassword.length() != 64){
            return false;
        }
        String salt = finalPassword.substring(0, 32);
        String securityPassword = DigestUtils.md5DigestAsHex((salt + password).getBytes());
        return (salt + securityPassword).equals(finalPassword);
    }
}
