package com.example.demo.mapper;

import com.example.demo.model.UserInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * Created with IntelliJ IDEA.
 * Description: 用户表 mapper
 * User: Administrator
 * Date: 2023-01-08
 * Time: 21:44
 */
@Mapper
public interface UserMapper {
    public Integer findName(@Param("username") String username);

    public int add(@Param("username") String username, @Param("password") String password);

    public UserInfo login(@Param("username") String username, @Param("password") String password);

    public UserInfo getUserByName(@Param("username") String username);

    public UserInfo myInfoById(@Param("uid") Integer uid);

    public int getTotalCreate(@Param("uid") Integer uid);

    public int updatePhoto(@Param("fileName") String fileName, @Param("id") Integer id);
}
