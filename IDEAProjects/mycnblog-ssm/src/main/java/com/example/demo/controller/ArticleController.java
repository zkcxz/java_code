package com.example.demo.controller;

import com.example.demo.common.AjaxResult;
import com.example.demo.common.Constant;
import com.example.demo.model.ArticleInfo;
import com.example.demo.model.DataInfo;
import com.example.demo.model.UserInfo;
import com.example.demo.service.ArticleService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Calendar;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description: 文章控制器
 * User: Administrator
 * Date: 2023-01-08
 * Time: 21:40
 */
@RestController
@RequestMapping("/art")
public class ArticleController {
    @Autowired
    private ArticleService articleService;

    @RequestMapping("/mylist")
    public List<ArticleInfo> myList(HttpServletRequest request){
        HttpSession session = request.getSession(false);
        if(session != null && session.getAttribute(Constant.SESSION_USERINFO_KEY) != null){
            UserInfo userInfo = (UserInfo) session.getAttribute(Constant.SESSION_USERINFO_KEY);
            return articleService.getMyList(userInfo.getId());
        }
        return null;
    }

    @RequestMapping("list")
    public List<ArticleInfo> getList(Integer pindex, Integer psize){
        if(pindex == null || psize == null){
            return null;
        }
        int offset = (pindex - 1) * psize;  //分页公式计算偏移量
        return articleService.getList(psize, offset);
    }

    @RequestMapping("/detail")
    public Object getDetil(Integer aid){
        if(aid != null && aid > 0){
            return AjaxResult.success(articleService.getDetail(aid));
        }
        return AjaxResult.fail(-1, "查询失败");
    }

    @RequestMapping("/edit")
    public Object edit(HttpServletRequest request, Integer aid){
        if(aid != null && aid > 0){
            return AjaxResult.success(articleService.getDetail(aid));
        }
        return AjaxResult.fail(-1, "查询失败");
    }

    @RequestMapping("/submit")
    public int update(HttpServletRequest request, Integer aid, String title, String content){
        HttpSession session = request.getSession(false);
        if(session != null && session.getAttribute(Constant.SESSION_USERINFO_KEY) != null){
            if(aid != null && aid > 0){
                return articleService.update(aid, title, content);
            }else{
                UserInfo userInfo = (UserInfo) session.getAttribute(Constant.SESSION_USERINFO_KEY);
                return articleService.addArt(userInfo.getId(), title, content);
            }
        }
        return 0;
    }

    @RequestMapping("/deletebyid")
    public Object deleteById(Integer aid){
        if(aid != null && aid > 0){
            return AjaxResult.success(articleService.deleteById(aid));
        }
        return AjaxResult.fail(-1, "删除失败");
    }

    @RequestMapping("/totalpage")
    public Integer getTotalCount(Integer psize){
        if(psize != null){
            int totalCount = articleService.getTotalCount();
            return (totalCount + psize) / psize;
        }
        return null;
    }

    @RequestMapping("/mycreate")
    public Integer getCreateCount(HttpServletRequest request){
        HttpSession session = request.getSession(false);
        if(session != null && session.getAttribute(Constant.SESSION_USERINFO_KEY) != null){
            int uid = ((UserInfo) session.getAttribute(Constant.SESSION_USERINFO_KEY)).getId();
            return articleService.getCreateCount(uid);
        }
        return null;
    }

    @RequestMapping("/myrcount")
    public Object getReadCount(HttpServletRequest request){
        HttpSession session = request.getSession(false);
        if(session != null && session.getAttribute(Constant.SESSION_USERINFO_KEY) != null){
            int uid = ((UserInfo) session.getAttribute(Constant.SESSION_USERINFO_KEY)).getId();
//            Calendar calendar = Calendar.getInstance();
//            for(int i = 2020; i <= calendar.get(Calendar.YEAR); i++){
//
//            }
            return articleService.getReadCount(uid);
        }
        return null;
    }

    @RequestMapping("/mydata")
    public List<DataInfo> mydata(HttpServletRequest request){
        HttpSession session = request.getSession(false);
        if(session != null && session.getAttribute(Constant.SESSION_USERINFO_KEY) != null){
            int uid = ((UserInfo) session.getAttribute(Constant.SESSION_USERINFO_KEY)).getId();
            return articleService.mydata(uid);
        }
        return null;
    }

    @RequestMapping("/mycreatebyid")
    public Integer getCreateCountById(Integer uid){
        if(uid != null && uid > 0){
            return articleService.getCreateCount(uid);
        }
        return null;
    }

    @RequestMapping("/myrcountbyid")
    public Integer getReadCountById(Integer uid){
        if(uid != null && uid > 0){
            return articleService.getReadCount(uid);
        }
        return null;
    }

    @RequestMapping("/addrcount")
    public Object addrcount(Integer aid){
        if(aid != null && aid > 0){
            int newrcount = articleService.getReadCountByAid(aid) + 1;
            return articleService.addrcount(aid, newrcount);
        }
        return null;
    }

    @RequestMapping("/searcher")
    public List<ArticleInfo> search(String opt, String query) {
        if(opt.equals("time")) {
            return articleService.getSearcherListBytime(query);
        } else {
            return articleService.getSearcherListBynums(query);
        }
//        List<Result> results = docSearcher.search(query);
//        return objectMapper.writeValueAsString(results);
    }

    @RequestMapping("/gettopid")
    public ArticleInfo gettopid(Integer aid){
        System.out.println("111");
        return articleService.gettopid(aid);
    }

    @RequestMapping("/top1byaid")
    public Object top1byaid(Integer aid){
        if(aid != null && aid > 0){
            return AjaxResult.success(articleService.top1byaid(aid));
        }
        return AjaxResult.fail(-1, "置顶失败");
    }

    @RequestMapping("/top0byaid")
    public Object top0byaid(Integer aid){
        if(aid != null && aid > 0){
            return AjaxResult.success(articleService.top0byaid(aid));
        }
        return AjaxResult.fail(-1, "取消置顶失败");
    }
}
