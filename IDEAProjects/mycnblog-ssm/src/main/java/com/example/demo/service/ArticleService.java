package com.example.demo.service;

import com.example.demo.mapper.ArticleMapper;
import com.example.demo.model.ArticleInfo;
import com.example.demo.model.DataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description: 文章表的服务层
 * User: Administrator
 * Date: 2023-01-08
 * Time: 21:43
 */
@Service
public class ArticleService {
    @Autowired
    private ArticleMapper articleMapper;

    public List<ArticleInfo> getMyList(Integer uid){
        return articleMapper.getMyList(uid);
    }

    public ArticleInfo getDetail(Integer aid){
        return articleMapper.getDetail(aid);
    }

    public int deleteById(Integer aid){
        return articleMapper.deleteById(aid);
    }

    public int update(Integer aid, String title, String content){
        return articleMapper.update(aid, title, content);
    }

    public int addArt(Integer uid, String title, String content){
        return articleMapper.addArt(uid, title, content);
    }

    public List<ArticleInfo> getList(Integer psize, Integer offset){
        return articleMapper.getList(psize, offset);
    }

    public int getTotalCount(){
        return articleMapper.getTotalCount();
    }

    public int getCreateCount(Integer uid){
        return articleMapper.getCreateCount(uid);
    }

    public int getReadCount(Integer uid){
        return articleMapper.getReadCount(uid);
    }

    public int getReadCountByAid(Integer aid){
        return articleMapper.getReadCountByAid(aid);
    }

    public int addrcount(Integer aid, Integer newrcount){
        return articleMapper.addrcount(aid, newrcount);
    }

    public List<ArticleInfo> getSearcherList(String query){
        return articleMapper.getSearcherList(query);
    }

    public ArticleInfo gettopid(Integer aid) {
        return articleMapper.gettopid(aid);
    }

    public int top1byaid(Integer aid) {
        return articleMapper.top1byaid(aid);
    }

    public int top0byaid(Integer aid) {
        return articleMapper.top0byaid(aid);
    }

    public List<DataInfo> mydata(int uid) {
        return articleMapper.mydata(uid);
    }

    public List<ArticleInfo> getSearcherListBytime(String query) {
        return articleMapper.getSearcherListBytime(query);
    }

    public List<ArticleInfo> getSearcherListBynums(String query) {
        return articleMapper.getSearcherListBynums(query);
    }
}
