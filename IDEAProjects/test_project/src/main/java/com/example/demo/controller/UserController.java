package com.example.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-01-06
 * Time: 18:03
 */
@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {
    @RequestMapping("/login")
    public boolean login(HttpServletRequest request, String username, String password) {
        //非空判断
//        if(username != null && username != "" && password != null && password != "")
        if(StringUtils.hasLength(username) && StringUtils.hasLength(password)) {
            //验证用户名密码是否正确
            if(username.equals("admin") && password.equals("admin")) {
                HttpSession session = request.getSession();
                session.setAttribute("userinfo", "admin");
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    @RequestMapping("/getinfo")
    public String getInfo(){
        log.debug("执行了getinfo方法");
        return "执行了getinfo方法";
    }

    @RequestMapping("/register")
    public String register(){
        log.debug("执行了register方法");
        return "执行了register方法";
    }
}
