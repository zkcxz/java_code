package com.example.demo.config;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * Description: 统一处理异常
 * User: Administrator
 * Date: 2023-01-06
 * Time: 20:51
 */
@ControllerAdvice
public class ErrorAdvice {
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public HashMap<String, Object> exAdvice(Exception exception){
        HashMap<String, Object> res = new HashMap<>();
        res.put("code", "-1");
        res.put("message", exception.getMessage());
        return res;
    }
}
