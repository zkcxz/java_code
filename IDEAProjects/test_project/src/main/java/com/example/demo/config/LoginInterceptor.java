package com.example.demo.config;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created with IntelliJ IDEA.
 * Description: 登录拦截器
 * User: Administrator
 * Date: 2023-01-06
 * Time: 18:20
 */
@Component
public class LoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //登录判断业务
        HttpSession session = request.getSession(false);
        if(session != null && session.getAttribute("userinfo") != null){
            return true;
        }
        response.setStatus(401);
        return false;
    }
}
