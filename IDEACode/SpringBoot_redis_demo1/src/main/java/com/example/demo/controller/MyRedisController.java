package com.example.demo.controller;

import com.example.demo.model.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-02-08
 * Time: 22:14
 */
@RestController
public class MyRedisController {
    private User user;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    @RequestMapping("/setstr")
    public String setStr(String key, String val){
        if(StringUtils.hasLength(key) && StringUtils.hasLength(val)){
            //将字符串存储到redis中
            stringRedisTemplate.opsForValue().set(key, val);
            return "操作成功！";
        }else{
            return "参数有误！";
        }
    }

    @RequestMapping("/getstr")
    public String getStr(String key){
        String res = null;
        if(StringUtils.hasLength(key)){
            res = stringRedisTemplate.opsForValue().get(key);
        }
        return res;
    }

    //单例懒汉模式
    public User getUser(){
        if(user == null){
            synchronized (this){
                if(user == null){
                    user = new User();
                    user.setId(1);
                    user.setName("zhangsan");
                    user.setAge(18);
                }
            }
        }
        return user;
    }

    @RequestMapping("/setobj")
    public String setObj() throws JsonProcessingException {
        User user = getUser();
        String userStr = objectMapper.writeValueAsString(user);
        stringRedisTemplate.opsForValue().set("user", userStr);
        return "操作成功！";
    }

    @RequestMapping("/getobj")
    public User getObj(String key) throws JsonProcessingException {
        String userStr = stringRedisTemplate.opsForValue().get("user");
        User user = objectMapper.readValue(userStr, User.class);
        return user;
    }

//    public boolean setHash(){
//        User user = getUser();
//        stringRedisTemplate.opsForHash().put();
//        return true;
//    }
}
