package com.example.demo.model;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-02-08
 * Time: 23:49
 */
@Data
public class User {
    private int id;
    private String name;
    private int age;
}
