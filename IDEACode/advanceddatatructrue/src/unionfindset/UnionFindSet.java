package unionfindset;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-02-04
 * Time: 11:42
 */
public class UnionFindSet {
    private int[] elem;

    public UnionFindSet(int n){
        this.elem = new int[n];
        Arrays.fill(elem, -1);
    }

    //查找一个元素的根节点
    public int findRoot(int x){
        if(x < 0) return -1;
        while(this.elem[x] >= 0){
            x = this.elem[x];
        }
        return x;
    }

    //查找两个元素是否在同一个集合
    public boolean isSameUnionFindSet(int x, int y){
        int rootX = findRoot(x);
        int rootY = findRoot(y);
        if(rootX != -1 && rootY != -1 && rootX == rootY){
            return true;
        }
        return false;
    }

    //集合合并操作
    public void union(int x, int y){
        int rootX = findRoot(x);
        int rootY = findRoot(y);
        if(rootX == -1 || rootY == -1 || rootX == rootY) return;
        this.elem[rootX] += this.elem[rootY];
        this.elem[rootY] = rootX;
    }

    //获取集合的数量
    public int getCount(){
        int cnt = 0;
        for(int x : this.elem){
            if(x < 0) cnt++;
        }
        return cnt;
    }
}
