package avltree;

/**
 * Created with IntelliJ IDEA.
 * Description: AVL树
 * User: Administrator
 * Date: 2023-01-27
 * Time: 16:21
 */
public class AVLTree {
    static class TreeNode{
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode parent;
        public int bf;  //平衡因子

        public TreeNode(int val){
            this.val = val;
        }
    }

    public TreeNode root;  //定义根节点

    public boolean insert(int val){
        TreeNode node = new TreeNode(val);
        if(root == null){
            root = node;
            return true;
        }
        TreeNode parent = null;
        TreeNode cur = root;
        while(cur != null){
            if(cur.val < val){
                parent = cur;
                cur = cur.right;
            }else if(cur.val == val){
                return false;
            }else{
                parent = cur;
                cur = cur.left;
            }
        }
        if(parent.val < val){
            parent.right = node;
        }else{
            parent.left = node;
        }

        node.parent = parent;
        cur = node;

        //修改平衡因子
        while(parent != null){
            if(cur == parent.right){
                parent.bf++;
            }else{
                parent.bf--;
            }
            //确定是否需要继续向上调整
            if(parent.bf == 0){
                //当出现这种情况说明已经平衡
                break;
            }else if(parent.bf == 1 || parent.bf == -1){
                //当出现这种情况需要继续向上判断
                cur = parent;
                parent = cur.parent;
            }else{
                if(parent.bf == 2){  //右树高，需要降低树的高度
                    if(cur.bf == 1){
                        rotateLeft(parent);
                    }else{
                        //相当于cur.bf == -1
                        rotateRL(parent);
                    }
                }else{
                    //相当于parent.bf == -2  左树高，需要降低树的高度
                    if(cur.bf == 1){
                        //左右双旋
                        rotateLR(parent);
                    }else{
                        //相当于cur.bf == -1
                        //右单旋
                        rotateRight(parent);
                    }
                }
                //旋转完后一定就是平衡的了
                break;
            }
        }
        return true;
    }

    //右左双旋
    private void rotateRL(TreeNode parent) {
        TreeNode subR = parent.right;
        TreeNode subRL = subR.left;
        int bf = subRL.bf;

        //先右旋
        rotateRight(parent.right);
        //再左旋
        rotateLeft(parent);

        //调整平衡因子
        if(bf == 1){
            parent.bf = -1;
            subR.bf = 0;
            subRL.bf = 0;
        }else if(bf == -1){
            parent.bf = 0;
            subR.bf = 1;
            subRL.bf = 0;
        }
    }

    //左右双旋
    private void rotateLR(TreeNode parent) {
        TreeNode subL = parent.left;
        TreeNode subLR = subL.right;
        int bf = subLR.bf;

        //先左旋
        rotateLeft(parent.left);
        //再右旋
        rotateRight(parent);

        //调整平衡因子
        if(bf == -1){
            subL.bf = 0;
            subLR.bf = 0;
            parent.bf = 1;
        }else if(bf == 1){
            subL.bf = -1;
            subLR.bf = 0;
            parent.bf = 0;
        }
    }

    //左单旋
    private void rotateLeft(TreeNode parent) {
        TreeNode subR = parent.right;
        TreeNode subRL = subR.left;
        TreeNode pParent = parent.parent;

        //修改指向
        parent.right = subRL;
        subR.left = parent;
        if(subRL != null){
            subRL.parent = parent;
        }
        parent.parent = subR;
        if(parent == root){
            root = subR;
            subR.parent = null;
        }else{
            if(pParent.left == parent){
                pParent.left = subR;
                subR.parent = pParent;
            }else{
                pParent.right = subR;
                subR.parent = pParent;
            }
        }

        //调整平衡因子
        subR.bf = 0;
        parent.bf = 0;
    }

    //右单旋
    private void rotateRight(TreeNode parent) {
        TreeNode subL = parent.left;
        TreeNode subLR = subL.right;
        TreeNode pParent = parent.parent;

        //修改指向
        parent.left = subLR;
        subL.right = parent;
        if(subLR != null) {
            subLR.parent = parent;
        }
        parent.parent = subL;
        if(parent == root){
            root = subL;
            subL.parent = null;
        }else{
            if(pParent.left == parent){
                pParent.left = subL;
                subL.parent = pParent;
            }else{
                pParent.right = subL;
                subL.parent = pParent;
            }
        }

        //调整平衡因子
        subL.bf = 0;
        parent.bf = 0;
    }

    //中序遍历判断结果是否有序
    public void inorder(TreeNode root){
        if(root == null) return;
        inorder(root.left);
        System.out.println(root.val + " ");
        inorder(root.right);
    }

    //获取树的高度
    public int height(TreeNode root){
        if(root == null) return 0;
        int leftH = height(root.left);
        int rightH = height(root.right);
        return leftH > rightH ? leftH + 1 : rightH + 1;
    }

    //判断树是否平衡
    public boolean isBalanced(TreeNode root){
        if(root == null) return true;
        int leftH = height(root.left);
        int rightH = height(root.right);
        if(rightH - leftH != root.bf){
            System.out.println("节点：" + root.val + "平衡因子异常");
            return false;
        }
        return Math.abs(leftH - rightH) <= 1 && isBalanced(root.left) && isBalanced(root.right);
    }
}
