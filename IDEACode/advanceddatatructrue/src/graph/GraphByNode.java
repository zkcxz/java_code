package graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description: 使用邻接表存储图
 * User: Administrator
 * Date: 2023-02-26
 * Time: 16:02
 */
public class GraphByNode {
    static class Node{
        public int src;  //起始位置
        public int dest;  //目标位置
        public int weight;
        public Node next;

        public Node(int src, int dest, int weight){
            this.src = src;
            this.dest = dest;
            this.weight = weight;
        }
    }

    public Map<Character, Integer> mapV;
    public List<Node> edgeList;  //存储边
    public boolean isDirect;

    public GraphByNode(int size, boolean isDirect){
        this.mapV = new HashMap<>();
        this.edgeList = new ArrayList<>();
        for(int i = 0; i < size; i++){
            this.edgeList.add(null);
        }
        this.isDirect = isDirect;
    }

    public void initArrayV(char[] array){
        for(int i = 0; i < array.length; i++){
            mapV.put(array[i], i);
        }
    }

    public void addEdge(char srcV, char destV, int weight){
        int scrIndex = getIndexV(srcV);
        int destIndex = getIndexV(destV);
        addEdgeChild(scrIndex, destIndex, weight);
        if(isDirect == false){  //无向图需要添加两条边
            addEdgeChild(destIndex, scrIndex, weight);
        }
    }

    private void addEdgeChild(int srcIndex, int destIndex, int weight){
        Node cur = edgeList.get(srcIndex);
        while(cur != null){
            if(cur.dest == destIndex) return;
            cur = cur.next;
        }
        Node node = new Node(srcIndex, destIndex, weight);
        node.next = edgeList.get(srcIndex);
        edgeList.set(srcIndex, node);
    }

    //获取定点的下标
    private int getIndexV(char v){
        if(mapV.containsKey(v)){
            return mapV.get(v);
        }
        return -1;
    }

    //获取顶点的度
    public int getDevOfV(char v){
        int cnt = 0;
        int srcIndex = getIndexV(v);
        Node cur = edgeList.get(srcIndex);
        while(cur != null){
            cnt++;
            cur = cur.next;
        }
        if(isDirect){
            int destIndex = srcIndex;
            for(int i = 0; i < mapV.size(); i++){
                if(i == destIndex) continue;
                Node node = edgeList.get(i);
                while(node != null){
                    if(node.dest == destIndex){
                        cnt++;
                    }
                    node = node.next;
                }
            }
        }
        return cnt;
    }
}
