package graph;

import unionfindset.UnionFindSet;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description: 使用邻接矩阵存储图
 * User: Administrator
 * Date: 2023-02-26
 * Time: 15:15
 */
public class GraphByMatrix {
    private Map<Character, Integer> mapV;  //定点数组
    private int[][] matrix;  //邻接矩阵
    private boolean isDirect;  //是否是有向图

    /**
     * @param size 代表当前定点个数
     * @param isDirect 代表是否是有向图
     */
    public GraphByMatrix(int size, boolean isDirect){
        this.mapV = new HashMap<>();
        this.matrix = new int[size][size];
        for(int i = 0; i < size; i++){
            Arrays.fill(matrix[i], Integer.MAX_VALUE);
        }
        this.isDirect = isDirect;
    }

    public void initArrayV(char[] array){
        for(int i = 0; i < array.length; i++){
            mapV.put(array[i], i);
        }
    }

    public void addEdge(char srcV, char destV, int weight){
        int srcIndex = getIndexV(srcV);
        int destIndex = getIndexV(destV);
        matrix[srcIndex][destIndex] = weight;
        if(isDirect == false){
            matrix[destIndex][srcIndex] = weight;  //如果是无向图则需要进行对称
        }
    }

    //获取定点的下标
    private int getIndexV(char v){
        if(mapV.containsKey(v)){
            return mapV.get(v);
        }
        return -1;
    }

    //获取顶点的度
    public int getDevOfV(char v){
        int cnt = 0;
        int srcIndex = getIndexV(v);
        //出度
        for(int i = 0; i < mapV.size(); i++){
            if(matrix[srcIndex][i] != Integer.MAX_VALUE){
                cnt++;
            }
        }
        //有向图还需要计算入度
        if(isDirect){
            for(int i = 0; i < matrix.length; i++){
                if(srcIndex == i) continue;
                for(int j = 0; j < matrix[i].length; j++){
                    if(matrix[j][srcIndex] != Integer.MAX_VALUE){
                        cnt++;
                    }
                }
            }
        }
        return cnt;
    }

    //广度优先遍历
    public void bfs(char v){
        boolean[] visited = new boolean[mapV.size()];
        Queue<Integer> queue = new LinkedList<>();
        int srcIndex = getIndexV(v);
        queue.offer(srcIndex);
        while(!queue.isEmpty()){
            int top = queue.poll();
            for(Character key : mapV.keySet()){
                if(mapV.get(key) == top){
                    System.out.print(key + " ");
                }
            }
            visited[top] = true;
            for(int i = 0; i < matrix[0].length; i++){
                if(matrix[top][i] != Integer.MAX_VALUE && visited[i] == false){
                    queue.offer(i);
                    visited[i] = true;
                }
            }
        }
    }

    //深度优先遍历
    public void dfs(char v){
        boolean[] visited = new boolean[mapV.size()];
        int srcIndex = getIndexV(v);
        dfsChild(srcIndex, visited);
    }

    private void dfsChild(int srcIndex, boolean[] visited){
        for(Character key : mapV.keySet()){
            if(mapV.get(key) == srcIndex){
                System.out.print(key + " ");
            }
        }
        visited[srcIndex] = true;
        for(int i = 0; i < matrix[0].length; i++){
            if(matrix[srcIndex][i] != Integer.MAX_VALUE && visited[i] == false){
                dfsChild(i, visited);
            }
        }
    }

    static class Edge{
        public int srcIndex;
        public int destIndex;
        public int weight;

        public Edge(int srcIndex, int destIndex, int weight){
            this.srcIndex = srcIndex;
            this.destIndex = destIndex;
            this.weight = weight;
        }
    }

    //最小生成树(返回权值总和)
    public int kruskal(GraphByMatrix minTree){
        PriorityQueue<Edge> minQueue = new PriorityQueue<>(new Comparator<Edge>() {
            @Override
            public int compare(Edge o1, Edge o2) {
                return o1.weight - o2.weight;
            }
        });
        //遍历邻接矩阵，将边存到优先级队列中
        for(int i = 0; i < matrix.length; i++){
            for(int j = 0; j < matrix[0].length; j++){
                if(i < j && matrix[i][j] != Integer.MAX_VALUE){
                    minQueue.offer(new Edge(i, j, matrix[i][j]));
                }
            }
        }
        UnionFindSet unionFindSet = new UnionFindSet(matrix.length);
        int size = 0;
        int totalWeight = 0;
        while(size < matrix.length && !minQueue.isEmpty()){
            Edge edge = minQueue.poll();
            int srcIndex = edge.srcIndex;
            int destIndex = edge.destIndex;
            if(!unionFindSet.isSameUnionFindSet(srcIndex, destIndex)){
                minTree.addEdgeByIndex(srcIndex, destIndex, edge.weight);
                size++;  //记录下添加边的条数
                totalWeight += edge.weight;
                unionFindSet.union(srcIndex, destIndex);
            }
        }
        if(size == matrix.length - 1){
            return totalWeight;
        }
        return -1;  //没有最小生成树
    }

    private void addEdgeByIndex(int srcIndex, int destIndex, int weight){
        matrix[srcIndex][destIndex] = weight;
        if(isDirect == false){
            matrix[destIndex][srcIndex] = weight;  //如果是无向图则需要进行对称
        }
    }

    public int prim(GraphByMatrix minTree, char chV){
        int srcIndex = getIndexV(chV);
        Set<Integer> setX = new HashSet<>();  //存储已经确定的点
        setX.add(srcIndex);
        Set<Integer> setY = new HashSet<>();  //存储不确定的点
        int n = matrix.length;
        for(int i = 0; i < n; i++){
            if(i != srcIndex) setY.add(i);
        }
        PriorityQueue<Edge> minQueue = new PriorityQueue<>(new Comparator<Edge>() {
            @Override
            public int compare(Edge o1, Edge o2) {
                return o1.weight - o2.weight;
            }
        });
        for(int i = 0; i < n; i++){
            if(matrix[srcIndex][i] != Integer.MAX_VALUE){
                minQueue.offer(new Edge(srcIndex, i, matrix[srcIndex][i]));
            }
        }
        int size = 0;
        int totalWeight = 0;
        while(!minQueue.isEmpty()){
            Edge min = minQueue.poll();
            int srcI = min.srcIndex;
            int destI = min.destIndex;
            if(!setX.contains(destI)) {
                minTree.addEdgeByIndex(srcI, destI, min.weight);
                size++;
                totalWeight += min.weight;
                if(size == n - 1) return totalWeight;
                //更新集合
                setX.add(destI);
                setY.remove(destI);
                //将destI连接出去的所有边都放到优先级队列中
                for(int i = 0; i < n; i++){
                    if(matrix[destI][i] != Integer.MAX_VALUE && !setX.contains(i)){
                        minQueue.offer(new Edge(destI, i, matrix[destI][i]));
                    }
                }
            }
        }
        return -1;
    }

    /**
     * dijkstra算法求最短路问题
     * @param vSrc 指定起点
     * @param dest 距离数组
     * @param pPath 路径
     */
    public void dijkstra(char vSrc, int[] dest, int[] pPath){
        int srcIndex = getIndexV(vSrc);
        Arrays.fill(dest, Integer.MAX_VALUE);
        dest[srcIndex] = 0;
        Arrays.fill(pPath, -1);
        pPath[srcIndex] = 0;
        int n = matrix.length;
        boolean[] s = new boolean[n];

        for(int k = 0; k < n; k++){
            int min = Integer.MAX_VALUE;
            int u = srcIndex;
            for(int i = 0; i < n; i++){
                if(s[i] == false && dest[i] < min){
                    min = dest[i];
                    u = i;
                }
            }
            s[u] = true;
            //松弛u连接出去的所有顶点v
            for(int v = 0; v < n; v++){
                if(s[v] == false && matrix[u][v] != Integer.MAX_VALUE && dest[u] + matrix[u][v] < dest[v]){
                    dest[v] = dest[u] + matrix[u][v];
                    pPath[v] = u;
                }
            }
        }
    }

    /**
     * ford算法求最短路问题
     * @param vSrc
     * @param dest
     * @param pPath
     */
    public boolean ford(char vSrc, int[] dest, int[] pPath){
        int srcIndex = getIndexV(vSrc);
        Arrays.fill(dest, Integer.MAX_VALUE);
        dest[srcIndex] = 0;
        Arrays.fill(pPath, -1);
        pPath[srcIndex] = 0;

        int n = matrix.length;
        for(int k = 0; k < n; k++) {
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    if (matrix[i][j] != Integer.MAX_VALUE && dest[i] + matrix[i][j] < dest[j]) {
                        dest[j] = dest[i] + matrix[i][j];
                        pPath[j] = i;
                    }
                }
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (matrix[i][j] != Integer.MAX_VALUE && dest[i] + matrix[i][j] < dest[j]) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * floyd算法求最短路问题
     * @param dest
     * @param pPath
     */
    public void floyd(int[][] dest, int[][] pPath){
        int n = matrix.length;
        for(int i = 0; i < n; i++){
            Arrays.fill(dest[i], Integer.MAX_VALUE);
            Arrays.fill(pPath[i], -1);
        }
        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++){
                if(matrix[i][j] != Integer.MAX_VALUE){
                    dest[i][j] = matrix[i][j];
                    pPath[i][j] = i;
                }else{
                    pPath[i][j] = -1;
                }
                if(i == j){
                    dest[i][j] = 0;
                    pPath[i][j] = -1;
                }
            }
        }
        for(int k = 0; k < n; k++) {
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    if (dest[i][k] != Integer.MAX_VALUE && dest[k][j] != Integer.MAX_VALUE && dest[i][k] + dest[k][j] < dest[i][j]) {
                        dest[i][j] = dest[i][k] + dest[k][j];
                        //更新父节点下标
                        pPath[i][j] = pPath[k][j];
                    }
                }
            }
        }
    }
}
