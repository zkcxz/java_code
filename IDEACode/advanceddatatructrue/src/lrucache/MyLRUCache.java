package lrucache;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-02-05
 * Time: 17:54
 */
public class MyLRUCache {
    static class DLinkNode{
        public int key;
        public int value;
        public DLinkNode prev;
        public DLinkNode next;

        public DLinkNode(){

        }

        public DLinkNode(int key, int value){
            this.key = key;
            this.value = value;
        }
    }

    public DLinkNode head;  //双向链表的头结点
    public DLinkNode tail;  //双向链表的尾结点
    public int usedSize;  //当前链表中的有效节点个数
    public Map<Integer, DLinkNode> map;
    public int capacity;  //容量

    public MyLRUCache(int capacity){
        this.head = new DLinkNode();
        this.tail = new DLinkNode();
        this.map = new HashMap<>();
        this.capacity = capacity;
        head.next = tail;
        tail.prev = head;
    }

    //存储元素
    public void put(int key, int val){
        //查找key是否被存储过
        DLinkNode node = map.get(key);
        if(node == null){
            DLinkNode dLinkNode = new DLinkNode(key, val);
            map.put(key, dLinkNode);
            addToTail(dLinkNode);
            usedSize++;
            if(usedSize > capacity){
                map.remove(head.next.key);
                removeNode(head.next);
                usedSize--;
            }
        }else{
            node.value = val;
            moveToTail(node);
        }
    }

    //将当前节点移到尾巴节点
    private void moveToTail(DLinkNode node) {
        removeNode(node);
        addToTail(node);
    }

    //删除指定节点
    private void removeNode(DLinkNode node) {
        node.prev.next = node.next;
        node.next.prev = node.prev;
    }

    //添加节点到链表的尾部
    private void addToTail(DLinkNode node) {
        tail.prev.next = node;
        node.prev = tail.prev;
        node.next = tail;
        tail.prev = node;
    }

    //访问元素
    public int get(int key){
        DLinkNode node = map.get(key);
        if(node == null){
            return -1;
        }else{
            moveToTail(node);
            return node.value;
        }
    }
}
