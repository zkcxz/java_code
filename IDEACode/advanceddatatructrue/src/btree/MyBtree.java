package btree;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-02-23
 * Time: 20:23
 */
public class MyBtree {
    public static final int M = 3;

    static class BTreeNode{
        public int[] keys;  //关键字
        public BTreeNode[] subs;  //孩子
        public BTreeNode parent;  //父节点
        public int usedSize;  //关键字数量

        public BTreeNode(){
            this.keys = new int[M];
            this.subs = new BTreeNode[M + 1];
        }
    }

    public BTreeNode root;  //根节点

    //插入操作
    public boolean insert(int key){
        if(root == null){
            root = new BTreeNode();
            root.keys[0] = key;
            root.usedSize++;
            return true;
        }
        //首先查看当前树中是否存在key节点
        Pair<BTreeNode, Integer> pair = find(key);
        if(pair.value != -1){
            return false;
        }
        BTreeNode parent = pair.key;
        int index = parent.usedSize - 1;
        for(; index >= 0; index--){
            if(parent.keys[index] >= key){
                parent.keys[index + 1] = parent.keys[index];
            }else{
                break;
            }
        }
        parent.keys[index + 1] = key;
        parent.usedSize++;
        if(parent.usedSize >= M){
            split(parent);
            return true;
        }else{
            return true;
        }
    }

    //分裂当前节点
    private void split(BTreeNode cur) {
        BTreeNode newNode = new BTreeNode();
        BTreeNode parent = cur.parent;
        int mid = cur.usedSize >> 1;
        int i = mid + 1;
        int j = 0;
        for(; i < cur.usedSize; i++, j++){
            newNode.keys[j] = cur.keys[i];
            newNode.subs[j] = cur.subs[i];
            //记得更新父节点
            if(newNode.subs[j] != null){
                newNode.subs[j].parent = newNode;
            }
        }
        newNode.subs[j] = cur.subs[i];  //记得多拷贝一次
        //记得更新父节点
        if(newNode.subs[j] != null){
            newNode.subs[j].parent = newNode;
        }

        //更新新节点的参数
        newNode.parent = parent;
        newNode.usedSize = j;
        cur.usedSize = cur.usedSize - j - 1;

        //特殊处理根节点的情况
        if(cur == root){
            root = new BTreeNode();
            root.keys[0] = cur.keys[mid];
            root.subs[0] = cur;
            root.subs[1] = newNode;
            root.usedSize = 1;
            cur.parent = root;
            newNode.parent = root;
            return;
        }

        int endT = parent.usedSize - 1;
        int midVal = cur.keys[mid];
        for(; endT >= 0; endT--){
            if(parent.keys[endT] >= midVal){
                parent.keys[endT + 1] = parent.keys[endT];
                parent.subs[endT + 2] = parent.subs[endT + 1];
            }else{
                break;
            }
        }
        parent.keys[endT + 1] = midVal;
        parent.subs[endT + 2] = newNode;
        parent.usedSize++;
        if(parent.usedSize >= M){
            split(parent);
        }
    }

    //查找元素是否在树中存在
    private Pair<BTreeNode, Integer> find(int key) {
        BTreeNode cur = root;
        BTreeNode parent = null;
        while(cur != null){
            int i = 0;
            while(i < cur.usedSize){
                if(cur.keys[i] == key){
                    return new Pair<>(cur, i);
                }else if(cur.keys[i] < key){
                    i++;
                }else{
                    break;
                }
            }
            parent = cur;
            cur = cur.subs[i];
        }
        return new Pair<>(parent, -1);
    }

    public static void main(String[] args) {
        MyBtree myBtree = new MyBtree();
        int[] a = {53, 139, 75, 49, 145, 36, 101};
        for(int i = 0; i < a.length; i++) myBtree.insert(a[i]);
        myBtree.inorder(myBtree.root);
    }

    private void inorder(BTreeNode root){
        if(root == null)
            return;
        for(int i = 0; i < root.usedSize; ++i){
            inorder(root.subs[i]);
            System.out.println(root.keys[i]);
        }
        inorder(root.subs[root.usedSize]);
    }
}
