package btree;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-02-25
 * Time: 14:44
 */
public class Pair<K, V> {
    public K key;
    public V value;

    public Pair(K key, V value){
        this.key = key;
        this.value = value;
    }
}
