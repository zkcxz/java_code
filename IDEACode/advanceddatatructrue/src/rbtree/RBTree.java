package rbtree;

import avltree.AVLTree;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-01-30
 * Time: 20:45
 */
public class RBTree {
    static class RBTreeNode{
        public RBTreeNode left;
        public RBTreeNode right;
        public RBTreeNode parent;
        public int val;
        public COLOR color;

        public RBTreeNode(int val){
            this.val = val;
            //新建的节点默认是红色的
            this.color = COLOR.RED;
        }
    }

    public RBTreeNode root;

    public boolean insert(int val){
        RBTreeNode node = new RBTreeNode(val);
        if(root == null){
            root = node;
            root.color = COLOR.BLACK;
            return true;
        }
        RBTreeNode parent = null;
        RBTreeNode cur = root;
        while(cur != null){
            if(cur.val < val){
                parent = cur;
                cur = cur.right;
            }else if(cur.val == val){
                return false;
            }else{
                parent = cur;
                cur = cur.left;
            }
        }
        if(parent.val < val){
            parent.right = node;
        }else{
            parent.left = node;
        }

        node.parent = parent;
        cur = node;

        //调整颜色
        while(parent != null && parent.color == COLOR.RED){
            RBTreeNode grandFather = parent.parent;  //grandFather不可能是空，因为红色节点必然有父节点
            if(parent == grandFather.left){
                RBTreeNode uncle = grandFather.right;
                if(uncle != null && uncle.color == COLOR.RED){
                    parent.color = COLOR.BLACK;
                    uncle.color = COLOR.BLACK;
                    grandFather.color = COLOR.RED;
                    //继续向上修改
                    cur = grandFather;
                    parent = cur.parent;
                }else{
                    //uncle不存在或者uncle的颜色为黑
                    //如果当前节点在父节点的右边，则先进行左单旋
                    if(cur == parent.right){
                        rotateLeft(parent);
                        //交换cur和parent
                        RBTreeNode tmp = parent;
                        parent = cur;
                        cur = tmp;
                    }
                    //不管当前节点在父节点的左边还是右边，都是需要进行右单旋并修改颜色
                    rotateRight(grandFather);
                    parent.color = COLOR.BLACK;
                    grandFather.color = COLOR.RED;
                }
            }else{
                //parent == grandFather.right
                RBTreeNode uncle = grandFather.left;
                if(uncle != null && uncle.color == COLOR.RED) {
                    parent.color = COLOR.BLACK;
                    uncle.color = COLOR.BLACK;
                    grandFather.color = COLOR.RED;
                    //继续向上修改
                    cur = grandFather;
                    parent = cur.parent;
                }else{
                    //uncle不存在或者uncle的颜色为黑
                    //如果当前节点在父节点的右边，则先进行左单旋
                    if(cur == parent.left){
                        rotateRight(parent);
                        //交换cur和parent
                        RBTreeNode tmp = parent;
                        parent = cur;
                        cur = tmp;
                    }
                    //不管当前节点在父节点的左边还是右边，都是需要进行右单旋并修改颜色
                    rotateLeft(grandFather);
                    parent.color = COLOR.BLACK;
                    grandFather.color = COLOR.RED;
                }
            }
        }
        root.color = COLOR.BLACK;
        return true;
    }

    //左单旋
    private void rotateLeft(RBTreeNode parent) {
        RBTreeNode subR = parent.right;
        RBTreeNode subRL = subR.left;
        RBTreeNode pParent = parent.parent;

        //修改指向
        parent.right = subRL;
        subR.left = parent;
        if(subRL != null){
            subRL.parent = parent;
        }
        parent.parent = subR;
        if(parent == root){
            root = subR;
            subR.parent = null;
        }else{
            if(pParent.left == parent){
                pParent.left = subR;
                subR.parent = pParent;
            }else{
                pParent.right = subR;
                subR.parent = pParent;
            }
        }
    }

    //右单旋
    private void rotateRight(RBTreeNode parent) {
        RBTreeNode subL = parent.left;
        RBTreeNode subLR = subL.right;
        RBTreeNode pParent = parent.parent;

        //修改指向
        parent.left = subLR;
        subL.right = parent;
        if(subLR != null){
            subLR.parent = parent;
        }
        parent.parent = subL;
        if(parent == root){
            root = subL;
            subL.parent = null;
        }else{
            if(pParent.left == parent){
                pParent.left = subL;
                subL.parent = pParent;
            }else{
                pParent.right = subL;
                subL.parent = pParent;
            }
        }
    }

    //中序遍历判断结果是否有序
    public void inorder(RBTreeNode root){
        if(root == null) return;
        inorder(root.left);
        System.out.println(root.val + " ");
        inorder(root.right);
    }

    public boolean isValidRBTree(){
        if(root == null){
            return true;  //空树也是红黑树
        }
        if(root.color != COLOR.BLACK){
            System.out.println("根节点必须是黑色的！");
            return false;
        }
        int blackNum = getBlackNum(root);
        return checkRedColor(root) && checkBlackNum(root, 0, blackNum);
    }

    //获取其中一条路径上的黑色节点个数
    private int getBlackNum(RBTreeNode root) {
        int num = 0;
        while(root != null){
            if(root.color == COLOR.BLACK){
                num++;
            }
            root = root.left;
        }
        return num;
    }

    //判断每条路径上的黑色节点数量是否相同
    private boolean checkBlackNum(RBTreeNode root, int pathBlackNum, int blackNum) {
        if(root == null){
            return true;
        }
        if(root.color == COLOR.BLACK){
            pathBlackNum++;
        }
        if(root.left == null && root.right == null){
            if(pathBlackNum != blackNum){
                System.out.println("每条路径上的黑色节点数量不相同");
                return false;
            }
        }
        return checkBlackNum(root.left, pathBlackNum, blackNum) && checkBlackNum(root.right, pathBlackNum, blackNum);
    }

    //判断是否出现两个连续的红色节点
    private boolean checkRedColor(RBTreeNode root) {
        if(root == null){
            return true;
        }
        if(root.color == COLOR.RED){
            RBTreeNode parent = root.parent;
            if(parent.color == COLOR.RED){
                System.out.println("连续出现两个红色节点！");
                return false;
            }
        }
        return checkRedColor(root.left) && checkRedColor(root.right);
    }
}
