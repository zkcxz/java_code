package bitset;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-02-01
 * Time: 16:21
 */
public class MyBitSet {
    private byte[] elem;
    private int usedSize;

    public MyBitSet(){
        elem = new byte[1];
    }

    //传入给出的最大比特位
    public MyBitSet(int n){
        elem = new byte[n / 8 + 1];
    }

    //将数据对应的比特位置为1
    public void set(int val){
        if(val < 0){
            throw new IndexOutOfBoundsException();
        }
        int arrayIndex = val / 8;  //先确定在哪一个字节上
        int bitIndex = val % 8;  //确定在哪一个比特位上
        this.elem[arrayIndex] |= (1 << bitIndex);
        this.usedSize++;
    }

    //检查这个数是否存在
    public boolean get(int val){
        if(val < 0){
            throw new IndexOutOfBoundsException();
        }
        int arrayIndex = val / 8;  //先确定在哪一个字节上
        int bitIndex = val % 8;  //确定在哪一个比特位上
        if((this.elem[arrayIndex] & (1 << bitIndex)) != 0){
            return true;
        }
        return false;
    }

    //将数据的对应位置置为0
    public void reSet(int val){
        if(val < 0){
            throw new IndexOutOfBoundsException();
        }
        int arrayIndex = val / 8;  //先确定在哪一个字节上
        int bitIndex = val % 8;  //确定在哪一个比特位上
        byte t = this.elem[arrayIndex];
        this.elem[arrayIndex] &= ~(1 << bitIndex);
        if(t != this.elem[arrayIndex]){
            this.usedSize--;
        }
    }

    public int getUsedSize(){
        return this.usedSize;
    }
}
