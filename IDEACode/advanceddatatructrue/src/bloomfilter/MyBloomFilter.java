package bloomfilter;

import java.util.BitSet;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-02-01
 * Time: 23:33
 */
class SimpleHash{
    public int cap;  //当前容量
    public int seed;  //随机

    public SimpleHash(int cap, int seed){
        this.cap = cap;
        this.seed = seed;
    }

    //模拟HashMap实现哈希函数（根据seed的不同创建不同的哈希函数）
    final int hash(String key) {
        int h;
        return (key == null) ? 0 : this.seed * (this.cap - 1) * ((h = key.hashCode()) ^ (h >>> 16));
    }
}

public class MyBloomFilter {
    public static final int DEFAULT_SIZE = 1 << 20;

    private BitSet bitSet;
    private int usedSize;

    public static final int[] seeds = {5, 7, 11, 13, 27, 33};

    public SimpleHash[] simpleHashes;

    public MyBloomFilter(){
        bitSet = new BitSet(DEFAULT_SIZE);
        simpleHashes = new SimpleHash[seeds.length];
        for(int i = 0; i < simpleHashes.length; i++){
            simpleHashes[i] = new SimpleHash(DEFAULT_SIZE, seeds[i]);
        }
    }

    //添加数据到布隆过滤器
    public void add(String val){
        //将不同seed下的哈希函数分别处理当前的数据，并存储在位图中
        for(int i = 0; i < simpleHashes.length; i++){
            bitSet.set(simpleHashes[i].hash(val));
        }
        this.usedSize++;
    }

    //判断是否包含val，会出现一定的误判
    public boolean contains(String val){
        //将不同seed下的哈希函数分别判断bitSet对应位置上是否为1
        for(int i = 0; i < simpleHashes.length; i++){
            if(!bitSet.get(simpleHashes[i].hash(val))){
                return false;
            }
        }
        return true;
    }
}
