import java.util.*;

class Solution {
    public static void main(String[] args) {
        int[] nums = {1,3,1,1,2};
        distance(nums);
    }

    public static long[] distance(int[] nums) {
        int n = nums.length;
        long[] res = new long[n];
        Map<Integer, List<Integer>> map = new HashMap<>();
        for(int i = 0; i < n; i++){
            int x = nums[i];
            if(!map.containsKey(x)){
                List<Integer> list = new ArrayList<>();
                map.put(x, list);
            }
            map.get(x).add(i);
        }
        for(int i = 0; i < n; i++){
            int x = nums[i];
            if(map.get(x).size() <= 1 || res[i] > 0) continue;
            else{
                long[] s = new long[map.get(x).size()];
                s[0] = map.get(x).get(0);
                for(int j = 1; j < map.get(x).size(); j++){
                    s[j] = s[j - 1] + map.get(x).get(j);
                }
                for(int j = 0; j < map.get(x).size(); j++){
                    int t = map.get(x).get(j);
                    res[i] = (j + 1) * t - s[j] + s[map.get(x).size() - 1] - s[j] - (map.get(x).size() - 1 - j) * t;
                }
            }
        }
        return res;
    }
}