package J2019A;

import java.math.BigInteger;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-04-08
 * Time: 20:42
 */
public class Main {
    public static void main(String[] args) {
        BigInteger res = new BigInteger("1");
        for(int i = 1; i <= 100; i++){
            res = res.multiply(new BigInteger(i + ""));
            System.out.println(i + " " + res);
        }
    }
}
