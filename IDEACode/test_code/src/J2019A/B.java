package J2019A;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-03-28
 * Time: 16:10
 */
public class B {
    public static void main(String[] args) {
        int n = 20190321;
        int mod = 10000;
        long a = 1;
        long b = 1;
        long c = 1;
        while(n-- > 0){
            long tmp = (a + b + c) % mod;
            a = b;
            b = c;
            c = tmp;
        }
        System.out.println(c);
    }
}
