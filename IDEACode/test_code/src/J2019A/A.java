package J2019A;

import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-03-28
 * Time: 16:06
 */
public class A {
    public static void main(String[] args) {
        Set<Integer> set = new HashSet<>();
        set.add(2);
        set.add(0);
        set.add(1);
        set.add(9);
        long res = 0;
        for(int i = 1; i <= 2019; i++){
            String t = i + "";
            for(int j = 0; j < t.length(); j++){
                int tmp = (int)(t.charAt(j) - '0');
                if(set.contains(tmp)) {
                    res += i * i;
                    break;
                }
            }
        }
        System.out.println(res);
    }
}
