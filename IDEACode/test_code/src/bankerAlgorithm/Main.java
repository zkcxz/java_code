package bankerAlgorithm;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    private static boolean flag = false;  // 标记是否已经添加进程
    private static List<List<Integer>> Max = new ArrayList<>();  // 最大需求量
    private static List<List<Integer>> Allocation = new ArrayList<>();  // 已分配资源数
    private static List<List<Integer>> Need = new ArrayList<>();  // 剩余资源数
    private static List<Integer> Avalable = new ArrayList<>();  // 可用资源数

    public static void main(String[] args) {
        while (true) {
            int choice = menu();
            if (choice == 0) break;
            else if (choice == 1) addProcess();
            else if (choice == 2) showProcess();
            else if (choice == 3) execute();
            else if (choice == 4) removeProcess();
            else errorOperation();
        }
    }

    // 错误操作
    private static void errorOperation() {
        System.out.println("非法操作！请重新正确输入！");
    }

    // 清空进程
    private static void removeProcess() {
        flag = false;
        Max = new ArrayList<>();
        Allocation = new ArrayList<>();
        Need = new ArrayList<>();
        Avalable = new ArrayList<>();
        System.out.println("进程清空完成！");
    }

    // 执行银行家算法
    private static void execute() {
        if (!flag) {
            System.out.println("未添加进程！请先添加进程！");
            return;
        }
        Scanner scanner = new Scanner(System.in);
        int n = Max.get(0).size();
        while (true) {
            System.out.println("请输入可用资源数(" + n + ")：");
            String[] s = scanner.nextLine().split(" ");
            if (s.length != n) {
                System.out.println("输入可用资源数不匹配！请重新正确输入！");
            } else {
                for (int i = 0; i < n; i++) {
                    Avalable.add(Integer.parseInt(s[i]));
                }
                System.out.println("可用资源数录入成功！");
                break;
            }
        }
        showProcess();
        showAvalable();
        System.out.println();
        while (true) {
            System.out.println("******************************");
            System.out.println("1.死锁避免，检验是否安全");
            System.out.println("2.死锁检验");
            System.out.println("3.退出");
            System.out.println("******************************");
            int choice = scanner.nextInt();
            if (choice == 3) {
                Avalable = new ArrayList<>();
                break;
            } else if (choice == 1) {
                List<List<Integer>> backupNeed = new ArrayList<>();
                for (int i = 0; i < Need.size(); i++) {
                    List<Integer> tmp = new ArrayList<>();
                    for (int j = 0; j < n; j++) {
                        tmp.add(Need.get(i).get(j));
                    }
                    backupNeed.add(tmp);
                }
                List<Integer> backupAvalable = new ArrayList<>();
                for (int i = 0; i < Avalable.size(); i++) {
                    backupAvalable.add(Avalable.get(i));
                }
                noProgramRequestsResources();
                Need = new ArrayList<>();
                for (int i = 0; i < backupNeed.size(); i++) {
                    List<Integer> tmp = new ArrayList<>();
                    for (int j = 0; j < n; j++) {
                        tmp.add(backupNeed.get(i).get(j));
                    }
                    Need.add(tmp);
                }
                Avalable = new ArrayList<>();
                for (int i = 0; i < backupAvalable.size(); i++) {
                    Avalable.add(backupAvalable.get(i));
                }
            } else if (choice == 2) {
                List<List<Integer>> backupAllocation = new ArrayList<>();
                for (int i = 0; i < Allocation.size(); i++) {
                    List<Integer> tmp = new ArrayList<>();
                    for (int j = 0; j < n; j++) {
                        tmp.add(Allocation.get(i).get(j));
                    }
                    backupAllocation.add(tmp);
                }
                List<List<Integer>> backupNeed = new ArrayList<>();
                for (int i = 0; i < Need.size(); i++) {
                    List<Integer> tmp = new ArrayList<>();
                    for (int j = 0; j < n; j++) {
                        tmp.add(Need.get(i).get(j));
                    }
                    backupNeed.add(tmp);
                }
                List<Integer> backupAvalable = new ArrayList<>();
                for (int i = 0; i < Avalable.size(); i++) {
                    backupAvalable.add(Avalable.get(i));
                }
                ProgramRequestsResources();
                Allocation = new ArrayList<>();
                for (int i = 0; i < backupAllocation.size(); i++) {
                    List<Integer> tmp = new ArrayList<>();
                    for (int j = 0; j < n; j++) {
                        tmp.add(backupAllocation.get(i).get(j));
                    }
                    Allocation.add(tmp);
                }
                Need = new ArrayList<>();
                for (int i = 0; i < backupNeed.size(); i++) {
                    List<Integer> tmp = new ArrayList<>();
                    for (int j = 0; j < n; j++) {
                        tmp.add(backupNeed.get(i).get(j));
                    }
                    Need.add(tmp);
                }
                Avalable = new ArrayList<>();
                for (int i = 0; i < backupAvalable.size(); i++) {
                    Avalable.add(backupAvalable.get(i));
                }
            } else System.out.println("非法操作！请重新正确输入！");
        }
    }

    // 有程序请求资源
    private static void ProgramRequestsResources() {
        Scanner scanner = new Scanner(System.in);
        int n = Max.size();
        while (true) {
            System.out.println("请输入要申请的第几个进程：");
            int t = scanner.nextInt();
            if (t < 1 || t > n) {
                System.out.println("不存在此进程，请重现输入(1~" + n + ")");
            } else {
                System.out.println("第" + t + "个进程提出请求(P" + (t - 1) + ")！");
                int m = Avalable.size();
                scanner.nextLine();
                while (true) {
                    System.out.println("输入要请求的资源Request：");
                    String[] s = scanner.nextLine().split(" ");
                    if (s.length != m) {
                        System.out.println("输入请求的资源不匹配！请重新正确输入！");
                    } else {
                        for (int i = 0; i < m; i++) {
                            int x = Integer.parseInt(s[i]);
                            Allocation.get(t - 1).set(i, Allocation.get(t - 1).get(i) + x);
                            Need.get(t - 1).set(i, Need.get(t - 1).get(i) - x);
                            Avalable.set(i, Avalable.get(i) - x);
                        }
                        break;
                    }
                }
                System.out.println("试分配如下：");
                showProcess();
                showAvalable();
                System.out.println();
                System.out.println("进行安全性判断：");
                noProgramRequestsResources();
                break;
            }
        }
    }

    //无程序请求资源
    private static void noProgramRequestsResources() {
        List<Integer> res = new ArrayList<>();
        int n = Max.size(), m = Avalable.size();
        boolean[] st = new boolean[n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (!st[j]) {
                    boolean flag = true;
                    for (int k = 0; k < m; k++) {
                        if (Need.get(j).get(k) > Avalable.get(k)) {
                            flag = false;
                            break;
                        }
                    }
                    if (flag) {
                        for (int k = 0; k < m; k++) {
                            Avalable.set(k, Avalable.get(k) + Allocation.get(j).get(k));
                        }
                        st[j] = true;
                        res.add(j);
                    }
                }
            }
        }
        if (res.size() == n) {
            System.out.println("存在一个安全序列，安全序列为：");
            for (int i = 0; i < res.size(); i++) {
                System.out.print("P" + res.get(i));
                if (i < res.size() - 1) System.out.print("-->");
            }
            System.out.println();
        } else {
            System.out.println("不存在任何一个安全序列！");
        }
    }

    // 显示可用资源数
    private static void showAvalable() {
        System.out.println("Avalable");
        for (int x : Avalable) System.out.print(x + " ");
    }

    // 显示进程
    private static void showProcess() {
        if (!flag) {
            System.out.println("当前未有进程！请先添加进程！");
            return;
        }
        System.out.println("Process     Max            Allocation     Need");
        int n = Max.size(), m = Max.get(0).size();
        for (int i = 0; i < n; i++) {
            String t = new String();
            t += "P" + i;
            t = String.format("%-12s", t);
            System.out.print(t);
            t = new String();
            for (int j = 0; j < m; j++) {
                t += Max.get(i).get(j) + " ";
            }
            t = String.format("%-15s", t);
            System.out.print(t);
            t = new String();
            for (int j = 0; j < m; j++) {
                t += Allocation.get(i).get(j) + " ";
            }
            t = String.format("%-15s", t);
            System.out.print(t);
            t = new String();
            for (int j = 0; j < m; j++) {
                t += Need.get(i).get(j) + " ";
            }
            System.out.println(t);
        }
        System.out.println("成功显示" + n + "个进程！");
    }

    // 添加进程
    private static void addProcess() {
        if (!flag) flag = true;
        else {
            System.out.println("程序中存在进程！请先清空进程！");
            return;
        }
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入系统中的【进程数】：");
        int numberByProcess = scanner.nextInt();
        int numberByResources = 0;
        while (numberByResources < 1 || numberByResources > 5) {
            System.out.println("请输入系统中的【资源种类数（≤5）】：");
            numberByResources = scanner.nextInt();
            if (numberByResources < 1) {
                System.out.println("输入资源种类数＜1，不符合要求！请重现正确输入！");
            } else if (numberByResources > 5) {
                System.out.println("输入资源种类数＞5，不符合要求！请重现正确输入！");
            }
        }
        for (int i = 0; i < numberByProcess; i++) {
            System.out.println("添加第" + (i + 1) + "个进程(P" + i + ")：");
            if (i == 0) scanner.nextLine();
            while (true) {
                System.out.println("请输入最大需求量：");
                String[] s = scanner.nextLine().split(" ");
                if (s.length != numberByResources) {
                    System.out.println("输入的资源种类不匹配！请重新正确输入！");
                } else {
                    List<Integer> tmp = new ArrayList<>();
                    for (int j = 0; j < s.length; j++) {
                        tmp.add(Integer.parseInt(s[j]));
                    }
                    Max.add(tmp);
                    break;
                }
            }
            while (true) {
                System.out.println("请输入已分配资源数：");
                String[] s = scanner.nextLine().split(" ");
                if (s.length != numberByResources) {
                    System.out.println("输入的资源种类不匹配！请重新正确输入！");
                } else {
                    List<Integer> tmp = new ArrayList<>();
                    for (int j = 0; j < s.length; j++) {
                        tmp.add(Integer.parseInt(s[j]));
                    }
                    Allocation.add(tmp);
                    break;
                }
            }
            List<Integer> tmp = new ArrayList<>();
            for (int j = 0; j < numberByResources; j++) {
                tmp.add(Max.get(i).get(j) - Allocation.get(i).get(j));
            }
            Need.add(tmp);
            System.out.println("第" + (i + 1) + "个进程(P" + i + ")添加成功！");
        }
        System.out.println("所有进程添加成功！");
    }

    // 打印菜单
    private static int menu() {
        System.out.println("******************************");
        System.out.println("*********模拟银行家算法*********");
        System.out.println("********* 1. 添加进程 *********");
        System.out.println("********* 2. 显示进程 *********");
        System.out.println("******* 3. 执行银行家算法 ******");
        System.out.println("********* 4. 清空进程 *********");
        System.out.println("********* 0. 退出程序 *********");
        System.out.println("******************************");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }
}
