/**
 * Created with IntelliJ IDEA.
 * Description: 前缀和算法模板
 * User: Administrator
 * Date: 2023-01-18
 * Time: 15:19
 */
public class prefixSum {
    //一维前缀和
    public void oneDimensionalPrefixSum(){
        int[] a = new int[10];  //原数组
        int[] s = new int[a.length + 1];  //前缀和数组
        for(int i = 1; i <= a.length; i++) s[i] = s[i - 1] + a[i];
        //求区间和（例如a[l] + ... + a[r]）
        //System.out.println(s[r] - s[l - 1]);
    }

    //二维前缀和
    public void twoDimensionalPrefixSum(){
        int[][] a = new int[10][10];  //原矩阵
        int[][] s = new int[10][10];  //前缀和矩阵
        for(int i = 1; i < 10; i++){
            for(int j = 1; j < 10; j++){
                s[i][j] = s[i - 1][j] + s[i][j - 1] - s[i - 1][j - 1] + a[i][j];
            }
        }
        //求矩阵区间和（例如以(x1, y1)为左上角，(x2, y2)为右下角的子矩阵的和）
        //System.out.println(s[x2][y2] - s[x2][y1 - 1] - s[x1 - 1][y2] + s[x1 - 1][y1 - 1]);
    }
}
