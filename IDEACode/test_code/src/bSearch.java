/**
 * Created with IntelliJ IDEA.
 * Description: 二分算法模板
 * User: Administrator
 * Date: 2023-01-18
 * Time: 15:02
 */
public class bSearch {
    public static boolean check(int x){
        return true;
    }

    //区域[l, r]被划分成[l, mid]和[mid + 1, r]时使用
    public static int bsearch_1(int l, int r){
        while(l < r){
            int mid = (l + r) >> 1;
            if(check(mid)) r = mid;
            else l = mid + 1;
        }
        return l;
    }

    //区域[l, r]被划分成[l, mid - 1]和[mid, r]时使用
    public static int bsearch_2(int l, int r){
        while(l < r){
            int mid = (l + r + 1) >> 1;
            if(check(mid)) l = mid;
            else r = mid - 1;
        }
        return l;
    }

    public static void main(String[] args) {

    }
}
