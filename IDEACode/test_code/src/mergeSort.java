/**
 * Created with IntelliJ IDEA.
 * Description: 归并排序求逆序对模板
 * User: Administrator
 * Date: 2023-01-18
 * Time: 12:35
 */
public class mergeSort {
    public static int res = 0;

    public static void merge_sort(int[] q, int l, int r, int[] tmp){
        if(l >= r) return;
        int mid = (l + r) >> 1;
        merge_sort(q, l, mid, tmp);
        merge_sort(q, mid + 1, r, tmp);
        int k = 0, i = l, j = mid + 1;
        while(i <= mid && j <= r){
            if(q[i] <= q[j]) tmp[k++] = q[i++];
            else{
                tmp[k++] = q[j++];
                res += mid - i + 1;
            }
        }
        while(i <= mid) tmp[k++] = q[i++];
        while(j <= r) tmp[k++] = q[j++];
        for(i = l, j = 0; i <= r; i++, j++){
            q[i] = tmp[j];
        }
    }

    public static void main(String[] args) {
        int[] q = new int[5];
        int[] tmp = new int[5];
        merge_sort(q, 0, q.length - 1, tmp);
    }
}
