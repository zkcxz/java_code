package SaiKr;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-02-25
 * Time: 11:50
 */
public class Backpack{
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        int V = scanner.nextInt();
        int[] v = new int[N + 1];
        int[] w = new int[N + 1];
        int[][] maxValues = new int[N + 1][V + 1];
        for(int i = 1; i <= N; i++){
            v[i] = scanner.nextInt();
            w[i] = scanner.nextInt();
        }
        for(int i = 1; i <= N; i++){
            for(int j = 1; j <= V; j++){
                if(v[i] > j){
                    maxValues[i][j] = maxValues[i - 1][j];
                }else{
                    maxValues[i][j] = Math.max(maxValues[i - 1][j], maxValues[i - 1][j - v[i]] + w[i]);
                }
            }
        }
        System.out.println(maxValues[N][V]);
    }
}
