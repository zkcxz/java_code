package SaiKr;

import java.util.Scanner;
import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-02-24
 * Time: 21:31
 */
public class postfixExpression {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        char[] s = scanner.next().toCharArray();
        Stack<Long> stack = new Stack<>();
        long t = 0;
        for(int i = 0; i < s.length; i++){
            if(s[i] == '+'){
                long b = stack.pop();
                long a = stack.pop();
                stack.push(a + b);
            }else if(s[i] == '-'){
                long b = stack.pop();
                long a = stack.pop();
                stack.push(a - b);
            }else if(s[i] == '*'){
                long b = stack.pop();
                long a = stack.pop();
                stack.push(a * b);
            }else if(s[i] == '/'){
                long b = stack.pop();
                long a = stack.pop();
                stack.push(a / b);
            }else if(s[i] == '.'){
                t = 0;
                continue;
            }else if(s[i] == '@'){
                break;
            }else{
                if(t != 0) stack.pop();
                t = t * 10 + (long)(s[i] - '0');
                stack.push(t);
            }
        }
        System.out.println(stack.pop());
    }
}
