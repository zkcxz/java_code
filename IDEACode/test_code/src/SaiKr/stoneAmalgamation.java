package SaiKr;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-02-25
 * Time: 11:03
 */
class Pair{
    public int index;
    public int sum;

    public Pair(int index, int sum){
        this.index = index;
        this.sum = sum;
    }
}

public class stoneAmalgamation {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        List<Integer> a = new ArrayList<>();
        List<Integer> b = new ArrayList<>();
        for(int i = 0; i < n; i++) {
            int x = scanner.nextInt();
            a.add(x);
            b.add(x);
        }
        int min = 0, max = 0;
        while(true) {
            if (a.size() <= 1) break;
            List<Pair> list = new ArrayList<>();
            for (int i = 0; i < a.size(); i++) {
                Pair pair;
                if (i == a.size() - 1) pair = new Pair(i, a.get(i) + a.get(0));
                else pair = new Pair(i, a.get(i) + a.get(i + 1));
                list.add(pair);
            }
            Collections.sort(list, (o1, o2) -> {
                return o1.sum - o2.sum;
            });
            min += list.get(0).sum;
            a.set(list.get(0).index, list.get(0).sum);
            a.remove((list.get(0).index + 1) % a.size());

        }
        System.out.println(min);
        while(true){
            if(b.size() <= 1) break;
            List<Pair> list = new ArrayList<>();
            for(int i = 0; i < b.size(); i++){
                Pair pair;
                if(i == b.size() - 1) pair = new Pair(i, b.get(i) + b.get(0));
                else pair = new Pair(i, b.get(i) + b.get(i + 1));
                list.add(pair);
            }
            Collections.sort(list, (o1, o2) -> {
                return o2.sum - o1.sum;
            });
            max += list.get(0).sum;
            b.set(list.get(0).index, list.get(0).sum);
            b.remove((list.get(0).index + 1) % b.size());

        }
        System.out.println(max);
    }
}
