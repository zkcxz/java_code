package SaiKr;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-02-24
 * Time: 21:42
 */
public class sort {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n];
        for(int i = 0; i < n; i++) a[i] = scanner.nextInt();
        Arrays.sort(a);
        for(int i = 0; i < n; i++){
            System.out.print(a[i]);
            if(i != n - 1) System.out.print(" ");
        }
        System.out.println();
    }
}
