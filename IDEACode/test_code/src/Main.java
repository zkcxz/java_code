import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.Scanner;

//public class Main {
//    public static int N = 10010;
//    public static int[] h = new int[N];
//    public static int[] e = new int[N];
//    public static int[] ne = new int[N];
//    public static int idx;
//    public static boolean[] st = new boolean[N];
//
//    public static void add(int a, int b){
//        e[idx] = b;
//        ne[idx] = h[a];
//        h[a] = idx++;
//    }
//
//    public static void bfs(int[] res, int u, int choice){
//        Deque<int[]> q = new ArrayDeque<>();
//        q.add(new int[]{u, -1});
//        while(!q.isEmpty()){
//            int[] p = q.poll();
//            int x = p[0], fa = p[1];
//            res[x - 1] = choice++;
//            st[x] = true;
//            if(choice > 4) choice = 1;
//            for(int i = h[x]; i != -1; i = ne[i]){
//                int j = e[i];
//                if(j == fa) continue;
//                q.add(new int[]{j, x});
//            }
//        }
//    }
//
//    public static int[] gardenNoAdj(int n, int[][] paths) {
//        Arrays.fill(h, -1);
//        for(int i = 0; i < paths.length; i++){
//            int x = paths[i][0], y = paths[i][1];
//            add(x, y);
//            add(y, x);
//        }
//        int[] res = new int[n];
//        for(int i = 0; i < n; i++){
//            if(st[i + 1]) continue;
//            bfs(res, i + 1, 1);
//        }
//        return res;
//    }
//
////    public static void main(String[] args) {
//////        int[][] a = {{1,2},{2,3},{3,1}};
//////        gardenNoAdj(3, a);
////    }
//
////    public static void main(String[] args) {
////        String name = "你好";
////        name = String.format("%-16s", name);
////        System.out.println(name+"length"+name.length());
////    }
//
//}


import java.util.*;

class Pair{
    public int u;
    public int fa;
    public Pair(int u, int fa){
        this.u = u;
        this.fa = fa;
    }
}

public class Main{
//    public static int N = 200010;
//    public static int[] h = new int[N];
//    public static int[] e = new int[2 * N];
//    public static int[] ne = new int[2 * N];
//    public static int idx = 0;
//    public static int[][] g = new int[N][N];
//
//    public static void add(int a, int b){
//        e[idx] = b;
//        ne[idx] = h[a];
//        h[a] = idx++;
//    }
//
//    public static void bfs(int u, int fa){
//        Deque<Pair> queue = new ArrayDeque<>();
//        queue.add(new Pair(u, fa));
//        int tmp = 1;
//        while(!queue.isEmpty()){
//            int size = queue.size();
//            while(size-- > 0){
//                Pair t = queue.poll();
//                for(int i = h[t.u]; i != -1; i = ne[i]){
//                    int j = e[i];
//                    if(j == t.fa) continue;
//                    g[t.u][j] = tmp;
//                    g[j][t.u] = tmp;
//                    queue.add(new Pair(j, t.u));
//                }
//            }
//            tmp++;
//        }
//    }

    public static void main(String[] args){
        int[] bh = {0,5,4,1,2,3};
        int res = 0;
        int tmp = bh.length;
        int min = bh[1];
        // System.out.println(tmp);
        // for(int i = 1; i < tmp - 1; i++) System.out.print(bh[i] + " ");
        for(int i = 2; i < tmp - 1; i++){
            if(bh[i] < min){
                res++;
                min = bh[i];
            }
        }
        System.out.println(res + 1);
    }
}