package GDCPC;

import java.math.BigInteger;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-04-07
 * Time: 11:08
 */
public class D {
//    public static BigInteger Fibonacci(long n) {
//        if(n == 0 || n == 1){
//            return new BigInteger(n + "");
//        }
//        //矩阵快速幂运算
//        BigInteger[][] t = {{new BigInteger("1"), new BigInteger("1")},{new BigInteger("1"), new BigInteger("0")}};
//        BigInteger[][] res = {{new BigInteger("1"), new BigInteger("0")},{new BigInteger("0"), new BigInteger("0")}};
//        long count =  n - 1;
//        while(count > 0) {
//            if((count & 1) == 1) res = mul(res, t); // 注意矩阵乘法顺序，不满足交换律
//            t = mul(t, t);
//            count >>= 1;
//        }
//        return res[0][0];
//    }
//
//    //两矩阵相乘运算
//    public static BigInteger[][] mul(BigInteger[][] a, BigInteger[][] b) {
//        // 矩阵乘法，新矩阵的行数 = a的行数rowa，列数 = b的列数colb
//        // a矩阵的列数 = b矩阵的行数 = common
//        int row = a.length, col = b[0].length, common = b.length;
//        BigInteger[][] res = new BigInteger[row][col];
//        for(int i = 0; i < row; i++){
//            for(int j = 0; j < col; j++){
//                res[i][j] = new BigInteger("0");
//            }
//        }
//        for (int i = 0; i < row; i++) {
//            for (int j = 0; j < col; j++) {
//                for (int k = 0; k < common; k++) {
//                    BigInteger tmp = a[i][k].multiply(b[k][j]);
//                    res[i][j] = res[i][j].add(tmp);
////                    res[i][j] += a[i][k] * b[k][j];
//                }
//            }
//        }
//        return res;
//    }

    public static long Fibonacci(int n) {
        if(n == 0 || n == 1){
            return n;
        }
        //矩阵快速幂运算
        long[][] t = {{1, 1},{1, 0}};
        long[][] res = {{1, 0},{0, 0}};
        int count =  n - 1;
        while(count > 0) {
            if((count & 1) == 1) res = mul(res, t); // 注意矩阵乘法顺序，不满足交换律
            t = mul(t, t);
            count >>= 1;
        }
        return res[0][0];
    }

    //两矩阵相乘运算
    public static long[][] mul(long[][] a, long[][] b) {
        // 矩阵乘法，新矩阵的行数 = a的行数rowa，列数 = b的列数colb
        // a矩阵的列数 = b矩阵的行数 = common
        int row = a.length, col = b[0].length, common = b.length;
        long[][] res = new long[row][col];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                for (int k = 0; k < common; k++) {
                    res[i][j] += a[i][k] * b[k][j];
                }
            }
        }
        return res;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        long n = scanner.nextLong();
        int i = 0;
        for(i = 2; i < 1000000; i++){
            if(Fibonacci(i) > n) break;
        }
        System.out.println(Fibonacci(i - 2) + " " + Fibonacci(i - 1));
    }
}
