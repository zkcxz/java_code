package GDCPC;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-04-07
 * Time: 20:33
 */
public class G {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        long[] a = new long[n + 1], b = new long[n + 1], c = new long[n + 1];
        long[] f = new long[n + 1];
        for(int i = 1; i <= n; i++){
            a[i] = scanner.nextLong();
            b[i] = scanner.nextLong();
            c[i] = scanner.nextLong();
        }
        Arrays.fill(f, 0x3f3f3f3f);
        f[1] = 0;
        for(int i = 1; i <= n; i++){
            for(int j = 1; j < i; j++){
                if((a[j] - a[i]) * (b[j] - b[i]) >= 0){
                    if(f[j] == 0x3f3f3f3f) continue;
                    f[i] = Math.min(f[i], f[j] + (a[j] * c[i] * c[i] + b[j] * c[i]));
                }
            }
        }
        System.out.println(f[n]);
    }
}
