package GDCPC;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-04-07
 * Time: 19:43
 */
public class H {
    public static String[] a = {"", "Zi", "Chou", "Yin", "Mao", "Chen", "Si", "Wu", "Wei", "Shen", "You", "Xu", "Hai"};
    public static String[] ben1 = {"- -", "---", "- -", "---", "- -", "---", "- -", "---"};
    public static String[] ben2 = {"- -", "---", "---", "- -", "- -", "---", "---", "- -"};
    public static String[] ben3 = {"- -", "---", "---", "---", "---", "- -", "- -", "- -"};

    public static void turn(String[] str, int t){
        char[] c = str[t].toCharArray();
        if(c[1] == '-') c[1] = ' ';
        else c[1] = '-';
        str[t] = new String(c);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String Y = scanner.next();
        int m = scanner.nextInt();
        int d = scanner.nextInt();
        String H = scanner.next();
        int y = 0, h = 0;
        for(int i = 0; i <= 12; i++){
            String x = a[i];
            if(Y.equals(x)) y = i;
            else if(H.equals(x)) h = i;
        }
        int t1 = (y + m + d) % 8;
        System.out.println(ben1[t1]);
        System.out.println(ben2[t1]);
        System.out.println(ben3[t1]);
        int t2 = (y + m + d + h) % 8;
        System.out.println(ben1[t2]);
        System.out.println(ben2[t2]);
        System.out.println(ben3[t2]);
        System.out.println();
        int t3 = (y + m + d + h) % 6;
        if(t3 == 0) turn(ben1, t1);
        else if(t3 == 1) turn(ben3, t2);
        else if(t3 == 2) turn(ben2, t2);
        else if(t3 == 3) turn(ben1, t2);
        else if(t3 == 4) turn(ben3, t1);
        else if(t3 == 5) turn(ben2, t1);
        System.out.println(ben1[t1]);
        System.out.println(ben2[t1]);
        System.out.println(ben3[t1]);
        System.out.println(ben1[t2]);
        System.out.println(ben2[t2]);
        System.out.println(ben3[t2]);
    }
}
