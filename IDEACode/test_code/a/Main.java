/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-08-06
 * Time: 9:24
 */
public class Main {
    /**
     * 在一个长度为n的数组里的所有数字都在0到n-1的范围内。 数组中某些数字是重复的，但不知道有几个数字是重复的。
     * 也不知道每个数字重复几次。请找出数组中任意一个重复的数字。 例如，如果输入长度为7的数组[2,3,1,0,2,5,3]，那么对应的输出是2或者3。
     * 存在不合法的输入的话输出-1
     */
    private static int duplicate(int[] nums){
        int len = nums.length;
        int[] a = new int[len + 10];
        for(int x : nums){
            a[x]++;
        }
        boolean flag = false;
        for(int i = 0; i < a.length; i++){
            flag = true;
            if(a[i] >= 2) System.out.print(i + " ");
        }
        if(flag == false) System.out.println("-1");
    }

    /**
     * 反转链表
     * 给你单链表的头节点head，请你反转链表，并返回反转后的链表
     */
    private static ListNode reverse(ListNode head){
        if(head.next == null) return head;
        ListNode t = head.next;
        head.next = null;
        while(t != null){
            ListNode tmp = t.text;
            t.next = head;
            head = t;
            t = tmp;
        }
        return head;
    }

//    学生表student中有如下几列：id（学号）、班级（class_id）、年龄（age）、性别（gender），
//    要求查出每个班级中年龄大于18岁的女生的数量
    select count(*) from student where gender='女' and age > 18 gourp by class_id;

//    修改表user中name中包含xxx的记录name改为yyy，只改1条
    update user set name='yyy' where name like '%xxx%' limit 1;

    public static void main(String[] args) {

    }
}
