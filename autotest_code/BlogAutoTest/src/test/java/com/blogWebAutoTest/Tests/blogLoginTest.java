package com.blogWebAutoTest.Tests;

import com.blogWebAutoTest.common.autoTestUtils;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.By;
import org.openqa.selenium.edge.EdgeDriver;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-05-30
 * Time: 15:16
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class blogLoginTest extends autoTestUtils {
    public static EdgeDriver driver;

    @BeforeAll
    static private void baseControl(){
        driver = createDriver();
        driver.get("http://43.139.71.60:8081/login.html");
    }

    /*
    * 页面是否正确打开
    * 检查点：主页、注册元素是否存在
    * */
    @Test
    @Order(1)
    public void loginPageLoadRight() throws IOException {
        driver.findElement(By.cssSelector("body > div.nav > a:nth-child(4)"));
        driver.findElement(By.cssSelector("body > div.nav > a:nth-child(5)"));
        getScreenShot(getClass().getName());
    }

    /*
    * 检查正常登录情况
    * */
    @ParameterizedTest
    @CsvSource({"root, 123456"})
    @Order(3)
    public void loginSuccess(String username, String password) throws InterruptedException, IOException {
        driver.findElement(By.cssSelector("#username")).clear();
        driver.findElement(By.cssSelector("#password")).clear();
        driver.findElement(By.cssSelector("#username")).sendKeys(username);
        driver.findElement(By.cssSelector("#password")).sendKeys(password);
        driver.findElement(By.cssSelector("#submit")).click();
        //对登录结果进行检查
//        Thread.sleep(3000);
        driver.findElement(By.cssSelector("body > div.nav > a:nth-child(5)"));
        getScreenShot(getClass().getName());
        driver.navigate().back();
//        Thread.sleep(3000);
    }

    /*
    * 检查异常的登录情况
    * */
    @ParameterizedTest
    @CsvSource({"root, 123"})
    @Order(2)
    public void loginFail(String username, String password) throws IOException, InterruptedException {
        driver.findElement(By.cssSelector("#username")).clear();
        driver.findElement(By.cssSelector("#password")).clear();
        driver.findElement(By.cssSelector("#username")).sendKeys(username);
        driver.findElement(By.cssSelector("#password")).sendKeys(password);
        driver.findElement(By.cssSelector("#submit")).click();
        //对登录结果进行检查
        Thread.sleep(3000);
        System.out.println(driver.switchTo().alert().getText());
        driver.switchTo().alert().accept();
        getScreenShot(getClass().getName());
//        Thread.sleep(3000);
    }
}
