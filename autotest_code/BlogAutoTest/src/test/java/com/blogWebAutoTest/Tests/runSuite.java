package com.blogWebAutoTest.Tests;

import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Administrator
 * Date: 2023-05-30
 * Time: 15:15
 */
@Suite
@SelectClasses({blogLoginTest.class, blogListTest.class, blogEditTest.class, blogDetailTest.class})
public class runSuite {
}
